install:
	@echo ----------------------------------------------------------------------
	@echo -- Environment installation starts
	@echo ----------------------------------------------------------------------
	docker-compose down -v
	docker-compose up -d
	docker exec -ti php /root/disable_xdebug.sh
	docker exec -ti php composer install
	docker exec -ti php bin/console doctrine:database:drop --force --if-exists -n -q
	docker exec -ti php bin/console doctrine:database:create -q
	docker exec -ti php bin/console doctrine:schema:drop --force -q -n
	docker exec -ti php bin/console doctrine:schema:create -q
	docker exec -ti php bin/console doctrine:fixtures:load --no-interaction -q
	docker-compose run wait
	docker exec -ti php bin/console app:es create
	docker-compose rm -svf wait
	@echo ----------------------------------------------------------------------
	@echo -- Environment installation ends successfully
	@echo ----------------------------------------------------------------------

start:
	@echo ----------------------------------------------------------------------
	@echo -- Starting Docker containers
	@echo ----------------------------------------------------------------------
	docker-compose up -d
	@echo ----------------------------------------------------------------------
	@echo -- Containers are ready
	@echo ----------------------------------------------------------------------

stop:
	@echo ----------------------------------------------------------------------
	@echo -- Stopping Docker containers
	@echo ----------------------------------------------------------------------
	docker-compose down -v
	@echo ----------------------------------------------------------------------
	@echo -- Containers are stopped
	@echo ----------------------------------------------------------------------

restart:
	make stop
	make start

enter_php:
	docker exec -ti php sh

cacl:
	docker exec -ti php bin/console ca:cl

cacl_force:
	docker exec -ti php rm -rf /var/www/html/var/cache/*
	docker exec -ti php bin/console ca:cl

enable_xdebug:
	docker exec -ti php /root/enable_xdebug.sh

disalbe_xdebug:
	docker exec -ti php /root/disable_xdebug.sh

#region test
test_unit:
	docker exec -ti php bin/phpunit --testsuite unit

test: test_unit

coverage:
	rm -rf ~/Downloads/tmp/coverage
	docker exec -ti php rm -rf /var/www/coverage
	docker exec -ti php /root/enable_xdebug.sh
	docker exec -ti php bin/phpunit --coverage-html /var/www/coverage
	docker exec -ti php /root/disable_xdebug.sh
	docker cp php:/var/www/coverage ~/Downloads/tmp/coverage
	open ~/Downloads/tmp/coverage/index.html
	docker exec -ti php rm -rf /var/www/coverage
#endregion test

#region database operations
drop_db:
	docker exec -ti php bin/console doctrine:schema:drop --force

create_db: drop_db
	docker exec -ti php bin/console doctrine:schema:create

fixture_load:
	docker exec -ti php bin/console doctrine:fixtures:load --no-interaction
#endregion database operations

#region code utils
stan:
	docker exec -ti php vendor/phpstan/phpstan/bin/phpstan analyse src --level=4

fixer:
	docker exec -ti php vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix src
#endregion code utils

#region elasticsearch
index_create:
	docker exec -ti php bin/console app:es create

index_delete:
	docker exec -ti php bin/console app:es delete

item_insert:
	docker exec -ti php bin/console app:es insert

item_search:
	docker exec -ti php bin/console app:es search
#endregion elasticsearch