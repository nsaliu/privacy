<?php

namespace App\Tests\Unit\Application\Helper;


use App\Application\Helper\JsonResponseHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class JsonResponseHelperTest extends TestCase
{
    /**
     * @var JsonResponseHelper
     */
    private $SUT;

    protected function setUp()
    {
        $this->SUT = new JsonResponseHelper();
    }

    public function testSetResponseShouldReturnJsonResponseHelperInstance()
    {
        $result = $this->SUT->setResponse(new JsonResponse());

        $this->assertInstanceOf(JsonResponseHelper::class, $result);
    }

    public function testSetMessage()
    {
        $expected = 'test';

        $this->SUT->setResponse(new JsonResponse());
        $this->SUT->setMessage($expected);

        $result = $this->SUT->writeResponse([], 200);
        $content = json_decode($result->getContent(), true);

        $this->assertEquals($expected, $content['message']);
    }

    public function testWriteResponse()
    {
        $expectedBody = '{"code":200,"data":[{"key":"value","key_1":{"key_2":"value 2"}}]}';
        $data = [
            'key' => 'value',
            'key_1' => [
                'key_2' => 'value 2'
            ]
        ];

        $this->SUT = new JsonResponseHelper();
        $this->SUT->setResponse(new JsonResponse());

        $result = $this->SUT->writeResponse($data, 200);

        $this->assertInstanceOf(JsonResponse::class, $result);
        $this->assertEquals($expectedBody, $result->getContent());
        $this->assertEquals(200, $result->getStatusCode());

        $resultAsArray = json_decode($result->getContent(), true);

        $this->assertInternalType('array', $resultAsArray);
        $this->assertCount(2, $resultAsArray);
        $this->assertArrayHasKey('code', $resultAsArray);
        $this->assertArrayHasKey('data', $resultAsArray);
        $this->assertArrayHasKey('key', $resultAsArray['data'][0]);
        $this->assertArrayHasKey('key_1', $resultAsArray['data'][0]);
        $this->assertArrayHasKey('key_2', $resultAsArray['data'][0]['key_1']);
    }
}