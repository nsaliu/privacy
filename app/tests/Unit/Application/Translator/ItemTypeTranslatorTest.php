<?php

namespace App\Tests\Unit\Application\Translator;

use App\Application\Dto\Input\InputItemTypeDto;
use App\Application\Dto\Output\ItemTypeDto;
use App\Application\Translator\ItemTypeTranslator;
use App\Domain\Entity\ItemType;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ItemTypeTranslatorTest extends  KernelTestCase
{
    /**
     * @var ItemTypeTranslator
     */
    private $SUT;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $mapper = self::$container->get(AutoMapperInterface::class);
        
        $this->SUT = new ItemTypeTranslator($mapper);
    }

    public function testFromInputItemTypesDtoToEntity()
    {
        $dtos = [
            new InputItemTypeDto(),
            new InputItemTypeDto(),
        ];

        $result = $this->SUT->fromInputItemTypesDtoToEntity($dtos);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(ItemType::class, $result[0]);
        $this->assertInstanceOf(ItemType::class, $result[1]);
    }

    public function testFromEntitiesToDtos()
    {
        $item_1 = new ItemType();
        $item_2 = new ItemType();

        $items = [
            $item_1,
            $item_2,
        ];

        $result = $this->SUT->fromEntitiesToDtos($items);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(ItemTypeDto::class, $result[0]);
        $this->assertInstanceOf(ItemTypeDto::class, $result[1]);
    }
}
