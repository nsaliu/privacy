<?php

namespace App\Tests\Unit\Application\Translator;

use App\Application\Dto\Input\CreateUserDto;
use App\Application\Translator\UserTranslator;
use App\Domain\Entity\User;
use App\Domain\Enum\RoleEnum;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTranslatorTest extends KernelTestCase
{
    /**
     * @var UserTranslator
     */
    private $SUT;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $mapper = self::$container->get(AutoMapperInterface::class);
        
        $this->SUT = new UserTranslator($mapper);
    }

    public function testFromEntitiesToDto()
    {
        $items = [
            new User(),
            new User(),
        ];

        $result = $this->SUT->fromEntitiesToDto($items);

        $this->assertInternalType('array', $result);
    }

    public function testFromCreateUserDtoToEntity()
    {
        $dto = new CreateUserDto();
        $dto->role = RoleEnum::ADMIN;

        $result = $this->SUT->fromCreateUserDtoToEntity($dto);

        $this->assertInstanceOf(User::class, $result);
    }
}
