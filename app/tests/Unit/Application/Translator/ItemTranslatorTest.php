<?php

namespace App\Tests\Unit\Application\Translator;

use App\Application\Dto\Input\Filter\ItemFindByFilterDto;
use App\Application\Dto\Input\InputItemDto;
use App\Application\Dto\Input\UpdateItemDto;
use App\Application\Dto\Output\ItemOutputDto;
use App\Application\Translator\ItemTranslator;
use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\ItemType;
use AutoMapperPlus\AutoMapperInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ItemTranslatorTest extends KernelTestCase
{
    /**
     * @var ItemTranslator
     */
    private $SUT;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $mapper = self::$container->get(AutoMapperInterface::class);
        
        $this->SUT = new ItemTranslator($mapper);
    }

    public function testFromUpdateItemDtoToItem()
    {
        $dto = new UpdateItemDto();
        $dto->channel_id = Uuid::uuid4()->toString();

        $this->assertInstanceOf(Item::class, $this->SUT->fromUpdateItemDtoToItem($dto));
    }

    public function testFromInputItemsDtoToEntities()
    {
        $item_1 = new InputItemDto();
        $item_1->item_type_id = Uuid::uuid4()->toString();

        $item_2 = new InputItemDto();
        $item_2->item_type_id = Uuid::uuid4()->toString();

        $dtos = [
            $item_1,
            $item_2,
        ];

        $this->assertInternalType('array', $this->SUT->fromInputItemsDtoToEntities($dtos));
    }

    public function testFromEntityToDto()
    {
        $itemType = new ItemType();
        $itemType->setId(Uuid::uuid4());
        $itemType->setName('test');

        $item = new Item();
        $item->setCreatedAt(new \DateTime());
        $item->setUpdatedAt(new \DateTime());
        $item->setType($itemType);

        $this->assertInstanceOf(ItemOutputDto::class, $this->SUT->fromEntityToDto($item));
    }

    public function testFromItemFindByFilterDtoToSearchItemFilter()
    {
        $this->assertInstanceOf(SearchItemFilter::class, $this->SUT->fromItemFindByFilterDtoToSearchItemFilter(new ItemFindByFilterDto()));
    }
}
