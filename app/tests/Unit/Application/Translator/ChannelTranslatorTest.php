<?php

namespace App\Tests\Unit\Application\Translator;


use App\Application\Dto\Input\InputChannelDto;
use App\Application\Dto\Output\ChannelOutputDto;
use App\Application\Dto\Output\ChannelOutputDtoLight;
use App\Application\Translator\ChannelTranslator;
use App\Domain\Entity\Channel;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ChannelTranslatorTest extends KernelTestCase
{
    /**
     * @var ChannelTranslator
     */
    private $SUT;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $mapper = self::$container->get(AutoMapperInterface::class);
        
        $this->SUT = new ChannelTranslator($mapper);
    }

    public function testFromEntityToDto()
    {
        $this->assertInstanceOf(ChannelOutputDto::class, $this->SUT->fromEntityToDto(new Channel()));
    }

    public function testFromEntitiesToDto()
    {
        $result = $this->SUT->fromEntitiesToDto([new Channel()]);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(ChannelOutputDto::class, $result[0]);
    }

    public function testFromInputChannelsDtoToEntities()
    {
        $dtos = [
            new InputChannelDto(),
            new InputChannelDto(),
        ];

        $result = $this->SUT->fromInputChannelsDtoToEntities($dtos);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(Channel::class, $result[0]);
        $this->assertInstanceOf(Channel::class, $result[1]);
    }

    public function testFromEntitiesToDtoLight()
    {
        $result = $this->SUT->fromEntitiesToDtoLight([new Channel()]);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(ChannelOutputDtoLight::class, $result[0]);
    }
}
