<?php

namespace App\Tests\Unit\Application\Translator;

use App\Application\Dto\Input\InputProjectDto;
use App\Application\Dto\Output\ProjectOutputDto;
use App\Application\Translator\ProjectTranslator;
use App\Domain\Entity\Project;
use AutoMapperPlus\AutoMapperInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProjectTranslatorTest extends KernelTestCase
{
    /**
     * @var ProjectTranslator 
     */
    private $SUT;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $mapper = self::$container->get(AutoMapperInterface::class);
        
        $this->SUT = new ProjectTranslator($mapper);
    }

    public function testFromInputProjectsDtoToEntities()
    {
        $dtos = [
            new InputProjectDto(),
            new InputProjectDto(),
        ];

        $result = $this->SUT->fromInputProjectsDtoToEntities($dtos);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(Project::class, $result[0]);
    }

    public function testFromEntitiesToDto()
    {
        $items = [
            new Project(),
            new Project(),
        ];

        $result = $this->SUT->fromEntitiesToDto($items);

        $this->assertInternalType('array', $result);
        $this->assertInstanceOf(ProjectOutputDto::class, $result[0]);
        $this->assertInstanceOf(ProjectOutputDto::class, $result[1]);
    }
}
