<?php

namespace App\Tests\Unit\Infrastructure\Doctrine\Generator;


use App\Domain\Entity\User;
use App\Infrastructure\Doctrine\Generator\UuidGenerator;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class UuidGeneratorTest extends TestCase
{
    public function testGenerateShouldReturnCurrentEntityId()
    {
        $uuid = Uuid::uuid4();

        $user = new User();
        $user->setId($uuid);

        $entityManagerMock = $this->createMock(EntityManager::class);

        $sut = new UuidGenerator();
        $result = $sut->generate(
            $entityManagerMock,
            $user
        );

        $this->assertNotNull($result);
        $this->assertInstanceOf(Uuid::class, $result);
        $this->assertEquals($uuid, $result);
    }

    public function testGenerateShouldReturnNewUuid()
    {
        $entityManagerMock = $this->createMock(EntityManager::class);

        $sut = new UuidGenerator();
        $result = $sut->generate(
            $entityManagerMock,
            null
        );

        $this->assertNotNull($result);
        $this->assertInstanceOf(Uuid::class, $result);
    }
}