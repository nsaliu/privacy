<?php

namespace App\Tests\Unit\Domain\Manager;


use App\Domain\Entity\Project;
use App\Domain\Entity\Role;
use App\Domain\Entity\User;
use App\Domain\Exception\RoleNotFoundException;
use App\Domain\Exception\UserAlreadyExistsException;
use App\Domain\Manager\UserManager;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\RoleRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class UserManagerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $userRepository;
    /**
     * @var MockObject
     */
    private $passwordEncoder;
    /**
     * @var MockObject
     */
    private $roleRepository;
    /**
     * @var MockObject
     */
    private $projectRepository;

    /**
     * @var UserManager
     */
    private $SUT;

    protected function setUp()
    {
        $this->userRepository = $this->createMock(UserRepositoryInterface::class);
        $this->passwordEncoder = $this->createMock(PasswordEncoderInterface::class);
        $this->roleRepository = $this->createMock(RoleRepositoryInterface::class);
        $this->projectRepository = $this->createMock(ProjectRepositoryInterface::class);

        $this->SUT = new UserManager(
            $this->userRepository,
            $this->passwordEncoder,
            $this->roleRepository,
            $this->projectRepository
        );
    }

    public function testListUsers()
    {
        $this->userRepository
            ->method('findAll')
            ->willReturn([new User(), new User()]);

        $result = $this->SUT->listUsers();

        $this->assertInternalType('array', $result);
    }

    public function testCreate()
    {
        $password = 'password_in_plain_text';
        $role = new Role();
        $role->setName('test_role');

        $user = new User();
        $user->setEmail('test@email.com');
        $user->setPassword($password);
        $user->setRole($role);

        $this->userRepository
            ->method('findOneBy')
            ->willReturn(null);

        $this->roleRepository
            ->method('findOneBy')
            ->willReturn($role);

        $this->passwordEncoder
            ->method('encodePassword')
            ->willReturn($password);

        $this->userRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $result = $this->SUT->create($user);

        $this->assertInstanceOf(User::class, $result);
        $this->assertEquals($password, $user->getPassword());
        $this->assertEquals($role->getName(), $result->getRole()->getName());
    }

    public function testCreateShouldRiseUserAlreadyExistsException()
    {
        $this->expectException(UserAlreadyExistsException::class);

        $user = new User();
        $user->setEmail('test@test.com');

        $this->userRepository
            ->method('findOneBy')
            ->willReturn($user);

        $this->SUT->create($user);
    }

    public function testCreateShouldRiseRoleNotFoundException()
    {
        $this->expectException(RoleNotFoundException::class);

        $role = new Role();
        $role->setName('test');

        $user = new User();
        $user->setEmail('test@test.com');
        $user->setRole($role);

        $this->userRepository
            ->method('findOneBy')
            ->willReturn(null);

        $this->roleRepository
            ->method('findOneBy')
            ->willReturn(null);

        $this->SUT->create($user);
    }

    public function testAddProjects()
    {
        $project = new Project();
        $project->setName('project 1');

        $user = new User();

        $this->projectRepository
            ->method('findOneBy')
            ->willReturn(null);

        $this->userRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $result = $this->SUT->addProjects($user, [$project]);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('created', $result);
        $this->assertArrayHasKey('already_exists', $result);
        $this->assertInternalType('array', $result['created']);
        $this->assertEquals($project->getId()->toString(), $result['created'][0]['id']);
        $this->assertEquals($project->getName(), $result['created'][0]['name']);
    }

    public function testFindByEmail()
    {
        $this->userRepository
            ->method('findOneBy')
            ->willReturn(new User());

        $result = $this->SUT->findByEmail('test@email.com');

        $this->assertInstanceOf(User::class, $result);
    }

    public function testFindById()
    {
        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $result = $this->SUT->findById(Uuid::uuid4());

        $this->assertInstanceOf(User::class, $result);
    }
}