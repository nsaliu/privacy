<?php

namespace App\Tests\Unit\Domain\Manager;


use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\User;
use App\Domain\Exception\ErrorDeletingItemsException;
use App\Domain\Exception\ItemNotFoundException;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Manager\ItemManager;
use App\Domain\Translator\ItemDocumentTranslator;
use App\Infrastructure\ElasticSearch\Entity\Filter\SearchItemDocumentFilter;
use App\Infrastructure\ElasticSearch\Entity\ItemDocument;
use App\Infrastructure\ElasticSearch\ESFactory;
use App\Infrastructure\ElasticSearch\Manager\ESDocumentManager;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ItemRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ItemManagerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $itemRepository;
    /**
     * @var MockObject
     */
    private $esFactory;
    /**
     * @var MockObject
     */
    private $itemDocumentTranslator;
    /**
     * @var MockObject
     */
    private $projectRepository;
    /**
     * @var MockObject
     */
    private $userRepository;
    /**
     * @var MockObject
     */
    private $channelRepository;

    /**
     * @var ItemManager
     */
    private $SUT;

    protected function setUp()
    {
        $this->itemRepository = $this->createMock(ItemRepositoryInterface::class);
        $this->esFactory = $this->createMock(ESFactory::class);
        $this->itemDocumentTranslator = $this->createMock(ItemDocumentTranslator::class);
        $this->projectRepository = $this->createMock(ProjectRepositoryInterface::class);
        $this->userRepository = $this->createMock(UserRepositoryInterface::class);
        $this->channelRepository = $this->createMock(ChannelRepositoryInterface::class);

        $this->SUT = new ItemManager(
            $this->itemRepository,
            $this->esFactory,
            $this->itemDocumentTranslator,
            $this->projectRepository,
            $this->userRepository,
            $this->channelRepository
        );
    }

    public function testFindByFilterWithResponseFromElasticSearch()
    {
        // prepare data
        $itemDocument = new ItemDocument(
            '',
            '',
            '',
            '',
            '',
            true,
            '',
            new \DateTime(),
            new \DateTime()
        );

        // configure mock
        $esDocumentManager = $this->createMock(ESDocumentManager::class);
        $esDocumentManager
            ->method('findByFilter')
            ->willReturn($itemDocument);

        $this->esFactory
            ->method('getDocumentManager')
            ->willReturn($esDocumentManager);

        $this->itemDocumentTranslator
            ->method('fromSearchItemFilterToSearchItemDocumentFilter')
            ->willReturn(new SearchItemDocumentFilter('', ''));
        $this->itemDocumentTranslator
            ->method('fromElasticSearchResultToItem')
            ->willReturn(new Item());

        $result = $this->SUT->findByFilter(new User(), new SearchItemFilter('', ''));

        // assertion
        $this->assertInstanceOf(Item::class, $result);
    }

    public function testFindByFilterWithResponseFromDatabase()
    {
        // configure mock
        $esDocumentManager = $this->createMock(ESDocumentManager::class);
        $esDocumentManager
            ->method('findByFilter')
            ->willReturn(null);

        $this->itemDocumentTranslator
            ->method('fromSearchItemFilterToSearchItemDocumentFilter')
            ->willReturn(new SearchItemDocumentFilter('', ''));

        $this->esFactory
            ->method('getDocumentManager')
            ->willReturn($esDocumentManager);

        $this->itemRepository
            ->method('findByFilter')
            ->willReturn(new Item());

        $result = $this->SUT->findByFilter(new User(), new SearchItemFilter('', ''));

        // assertion
        $this->assertInstanceOf(Item::class, $result);
    }

    public function testDeleteShouldRiseUserNotFoundException()
    {
        $this->expectException(UserNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->delete(Uuid::uuid4(), []);
    }

    public function testDeleteShouldRiseItemNotFoundException()
    {
        $this->expectException(ItemNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->itemRepository
            ->method('findOneBy')
            ->willReturn(null);

        $this->SUT->delete(Uuid::uuid4(), ['test_value']);
    }

    public function testDeleteShouldRiseErrorDeletingItemsException()
    {
        $this->expectException(ErrorDeletingItemsException::class);

        // prepare data
        $itemValues = [
            '',
            ''
        ];

        // configure mock
        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->itemRepository
            ->method('findOneBy')
            ->willReturn(new Item());
        $this->itemRepository
            ->expects($this->atLeast(1))
            ->method('delete');
        $this->itemRepository
            ->expects($this->atLeast(1))
            ->method('flush')
            ->willThrowException(new \Exception());

        $esDocumentManager = $this->createMock(ESDocumentManager::class);
        $esDocumentManager
            ->expects($this->never())
            ->method('delete');

        $this->itemDocumentTranslator
            ->method('fromSearchItemFilterToSearchItemDocumentFilter')
            ->willReturn(new SearchItemDocumentFilter('', ''));

        $this->esFactory
            ->method('getDocumentManager')
            ->willReturn($esDocumentManager);

        $result = $this->SUT->delete(Uuid::uuid4(), $itemValues);

        // assertion
        $this->assertTrue($result);
    }
    
    public function testDelete()
    {
        // prepare data
        $itemValues = [
            '',
            ''
        ];

        // configure mock
        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->itemRepository
            ->method('findOneBy')
            ->willReturn(new Item());
        $this->itemRepository
            ->expects($this->atLeast(1))
            ->method('delete');
        $this->itemRepository
            ->expects($this->atLeast(1))
            ->method('flush');

        $esDocumentManager = $this->createMock(ESDocumentManager::class);
        $esDocumentManager
            ->expects($this->once())
            ->method('delete');

        $this->itemDocumentTranslator
            ->method('fromSearchItemFilterToSearchItemDocumentFilter')
            ->willReturn(new SearchItemDocumentFilter('', ''));

        $this->esFactory
            ->method('getDocumentManager')
            ->willReturn($esDocumentManager);

        $result = $this->SUT->delete(Uuid::uuid4(), $itemValues);

        // assertion
        $this->assertTrue($result);
    }

    public function testUpdate()
    {
        $expected = new Item();
        $expected->setDescription('description to update');
        $expected->setAccepted(false);

        $itemDocument = new ItemDocument(
            '',
            '',
            '',
            '',
            '',
            true,
            '',
            new \DateTime(),
            new \DateTime()
        );

        $item = new Item();
        $item->setDescription('description');
        $item->setAccepted(true);
        $item->setValue('');

        $this->itemRepository
            ->method('findByChannelIdAndValue')
            ->willReturn($expected);

        $this->itemRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $esDocumentManager = $this->createMock(ESDocumentManager::class);
        $esDocumentManager
            ->expects($this->once())
            ->method('update');

        $this->esFactory
            ->method('getDocumentManager')
            ->willReturn($esDocumentManager);

        $this->itemDocumentTranslator
            ->method('fromEntityToDocument')
            ->willReturn($itemDocument);

        $result = $this->SUT->update($item, Uuid::uuid4());

        $this->assertInstanceOf(Item::class, $result);
        $this->assertEquals($item->getDescription(), $expected->getDescription());
        $this->assertEquals($item->isAccepted(), $expected->isAccepted());
    }

    public function testUpdateShouldRiseItemNotFoundException()
    {
        $this->expectException(ItemNotFoundException::class);

        $item = new Item();
        $item->setValue('test_value');

        $this->itemRepository
            ->method('findByChannelIdAndValue')
            ->willReturn(null);

        $this->SUT->update($item, '');
    }
}