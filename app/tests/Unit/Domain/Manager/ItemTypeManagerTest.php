<?php

namespace App\Tests\Unit\Domain\Manager;


use App\Domain\Entity\ItemType;
use App\Domain\Manager\ItemTypeManager;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ItemTypeManagerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $itemTypeRepository;

    /**
     * @var ItemTypeManager
     */
    private $SUT;

    protected function setUp()
    {
        $this->itemTypeRepository = $this->createMock(ItemTypeRepositoryInterface::class);

        $this->SUT = new ItemTypeManager(
            $this->itemTypeRepository
        );
    }

    public function testFindAll()
    {
        $this->itemTypeRepository
            ->method('findByProjectId')
            ->willReturn([new ItemType()]);

        $result = $this->SUT->findAll(Uuid::uuid4());

        $this->assertInternalType('array', $result);
    }
}