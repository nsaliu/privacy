<?php

namespace App\Tests\Unit\Domain\Manager;


use App\Domain\Entity\Channel;
use App\Domain\Entity\Item;
use App\Domain\Entity\ItemType;
use App\Domain\Entity\User;
use App\Domain\Exception\ChannelEntityNotFoundException;
use App\Domain\Exception\ItemMustHaveAnExistingItemTypeException;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Helper\ItemEncryptHelper;
use App\Domain\Manager\ChannelManager;
use App\Domain\Translator\ItemDocumentTranslator;
use App\Infrastructure\ElasticSearch\ESFactory;
use App\Infrastructure\ElasticSearch\Manager\ESDocumentManager;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use App\Infrastructure\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ChannelManagerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $channelRepository;
    /**
     * @var MockObject
     */
    private $itemTypeRepository;
    /**
     * @var MockObject
     */
    private $esManager;
    /**
     * @var MockObject
     */
    private $itemDocumentTranslator;
    /**
     * @var MockObject
     */
    private $itemRepository;
    /**
     * @var MockObject
     */
    private $userRepository;
    /**
     * @var MockObject
     */
    private $projectRepository;
    /**
     * @var MockObject
     */
    private $encryptHelper;

    /**
     * @var ChannelManager
     */
    private $SUT;

    protected function setUp()
    {
        $this->channelRepository = $this->createMock(ChannelRepositoryInterface::class);
        $this->itemTypeRepository = $this->createMock(ItemTypeRepositoryInterface::class);
        $this->esManager = $this->createMock(ESFactory::class);
        $this->itemDocumentTranslator = $this->createMock(ItemDocumentTranslator::class);
        $this->itemRepository = $this->createMock(ItemRepository::class);
        $this->userRepository = $this->createMock(UserRepositoryInterface::class);
        $this->projectRepository = $this->createMock(ProjectRepositoryInterface::class);
        $this->encryptHelper = $this->createMock(ItemEncryptHelper::class);

        $this->SUT = new ChannelManager(
            $this->channelRepository,
            $this->itemTypeRepository,
            $this->esManager,
            $this->itemDocumentTranslator,
            $this->itemRepository,
            $this->userRepository,
            $this->projectRepository,
            $this->encryptHelper
        );
    }

    public function testFindChannelsByProjectId()
    {
        $expected = [
            new Channel(),
            new Channel(),
        ];

        $this->channelRepository
            ->expects($this->once())
            ->method('findByProjectId')
            ->willReturn($expected);

        $result = $this->SUT->findChannelsByProjectId(Uuid::uuid4());

        $this->assertInternalType('array', $result);
    }

    public function testFindById()
    {
        $expected = new Channel();

        $this->channelRepository
            ->method('find')
            ->willReturn($expected);

        $result = $this->SUT->findById(Uuid::uuid4());

        $this->assertInstanceOf(Channel::class, $result);
    }

    public function testAddItemsShouldRiseChannelEntityNotFoundException()
    {
        $this->expectException(ChannelEntityNotFoundException::class);

        $this->channelRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->addItems(new User(), '', []);
    }

    public function testAddItemsShouldRiseItemMustHaveAnExistingItemTypeException()
    {
        $this->expectException(ItemMustHaveAnExistingItemTypeException::class);

        $itemType = new ItemType();
        $itemType->setName('test');

        $item = new Item();
        $item->setValue('test');
        $item->setType($itemType);

        $this->channelRepository
            ->method('find')
            ->willReturn(new Channel());

        $this->itemRepository
            ->method('findByChannelIdAndValue')
            ->willReturn(null);

        $this->itemTypeRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->addItems(new User(), '', [$item]);
    }

    public function testAddItems()
    {
        // prepare data
        $item_1_Uuid = Uuid::uuid4();
        $item_2_Uuid = Uuid::uuid4();

        $item1 = new Item();
        $item1->setId($item_1_Uuid);
        $item1->setValue('test_1');
        $item1->setType(new ItemType());

        $item2 = new Item();
        $item2->setId($item_2_Uuid);
        $item2->setValue('test_2');
        $item2->setType(new ItemType());

        $items = [
            $item1,
            $item2
        ];

        $channel = new Channel();

        // configure mock
        $user = $this->createMock(User::class);

        $this->channelRepository
            ->method('find')
            ->willReturn($channel);
        $this->channelRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $this->itemRepository
            ->method('findByChannelIdAndValue')
            ->willReturn(null);

        $this->itemTypeRepository
            ->method('find')
            ->willReturn(new ItemType());

        $this->encryptHelper
            ->method('encrypt')
            ->willReturn('test');

        $this->channelRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $esDocumentManager = $this->createMock(ESDocumentManager::class);
        $esDocumentManager
            ->expects($this->once())
            ->method('insertMultipleItems');

        $this->esManager
            ->method('getDocumentManager')
            ->willReturn($esDocumentManager);

        $this->itemDocumentTranslator
            ->expects($this->once())
            ->method('fromEntitiesToDocuments');

        $result = $this->SUT->addItems($user, $channel->getId(), $items);

        // assertion
        $this->assertInstanceOf(Channel::class, $result);
        $this->assertInstanceOf(ArrayCollection::class, $channel->getItems());
        $this->assertCount(2, $channel->getItems());
        $this->assertInstanceOf(Item::class, $channel->getItems()[0]);
        $this->assertInstanceOf(Item::class, $channel->getItems()[1]);
    }

    public function testDelete()
    {
        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->channelRepository
            ->method('find')
            ->willReturn(new Channel());
        $this->channelRepository
            ->expects($this->once())
            ->method('delete');
        $this->channelRepository
            ->expects($this->once())
            ->method('flush');

        $result = $this->SUT->delete(Uuid::uuid4(), Uuid::uuid4());

        $this->assertTrue($result);
    }

    public function testDeleteShouldRiseUserNotFoundException()
    {
        $this->expectException(UserNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->delete(Uuid::uuid4(), Uuid::uuid4());
    }

    public function testDeleteShouldRiseChannelEntityNotFoundException()
    {
        $this->expectException(ChannelEntityNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->channelRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->delete(Uuid::uuid4(), Uuid::uuid4());
    }
}