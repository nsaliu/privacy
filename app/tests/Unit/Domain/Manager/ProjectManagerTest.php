<?php

namespace App\Tests\Unit\Domain\Manager;


use App\Domain\Entity\Channel;
use App\Domain\Entity\ItemType;
use App\Domain\Entity\Project;
use App\Domain\Entity\User;
use App\Domain\Exception\ProjectNotFoundException;
use App\Domain\Exception\UserIsNotActiveExcption;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Manager\ProjectManager;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProjectManagerTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $projectRepository;
    /**
     * @var MockObject
     */
    private $userRepository;
    /**
     * @var MockObject
     */
    private $channelRepository;
    /**
     * @var MockObject
     */
    private $itemTypeRepository;

    /**
     * @var ProjectManager
     */
    private $SUT;

    protected function setUp()
    {
        $this->projectRepository = $this->createMock(ProjectRepositoryInterface::class);
        $this->userRepository = $this->createMock(UserRepositoryInterface::class);
        $this->channelRepository = $this->createMock(ChannelRepositoryInterface::class);
        $this->itemTypeRepository = $this->createMock(ItemTypeRepositoryInterface::class);

        $this->SUT = new ProjectManager(
            $this->projectRepository,
            $this->userRepository,
            $this->channelRepository,
            $this->itemTypeRepository
        );
    }

    public function testFindByUser()
    {
        $projects = [
            new Project(),
            new Project(),
        ];

        $user = new User();
        $user->setActive(true);

        $this->userRepository
            ->method('find')
            ->willReturn($user);

        $this->projectRepository
            ->method('findByUserId')
            ->willReturn($projects);

        $result = $this->SUT->findByUser($user->getId());

        $this->assertInternalType('array', $result);
    }

    public function testFindByUserShouldRiseUserNotFoundException()
    {
        $this->expectException(UserNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->findByUser('');
    }

    public function testFindByUserShouldRiseUserIsNotActiveException()
    {
        $this->expectException(UserIsNotActiveExcption::class);

        $user = new User();
        $user->setActive(false);

        $this->userRepository
            ->method('find')
            ->willReturn($user);

        $this->SUT->findByUser('');
    }

    public function testAddChannelsShouldRiseProjectNotFoundException()
    {
        $this->expectException(ProjectNotFoundException::class);

        $this->projectRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->addChannels(new User(), '', [new Channel()]);
    }

    public function testAddChannels()
    {
        $channel = new Channel();
        $channel->setName('test_channel');

        $project = new Project();

        $this->projectRepository
            ->method('find')
            ->willReturn($project);

        $this->channelRepository
            ->method('findByProjectIdAndChannelName')
            ->willReturn(null);

        $this->projectRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $result = $this->SUT->addChannels(new User(), '', [$channel]);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('created', $result);
        $this->assertArrayHasKey('already_exists', $result);
        $this->assertArrayHasKey('id', $result['created'][0]);
        $this->assertArrayHasKey('name', $result['created'][0]);
        $this->assertEquals($channel->getId()->toString(), $result['created'][0]['id']);
        $this->assertEquals($channel->getName(), $result['created'][0]['name']);
    }

    public function testAddItemTypesShouldRiseProjectNotFoundException()
    {
        $this->expectException(ProjectNotFoundException::class);

        $itemType = new ItemType();
        $itemType->setName('test_item_type');

        $this->projectRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->addItemTypes('', [$itemType]);
    }

    public function testAddItemTypes()
    {
        $itemType = new ItemType();
        $itemType->setName('test_item_type');

        $project = new Project();

        $this->projectRepository
            ->method('find')
            ->willReturn($project);

        $this->itemTypeRepository
            ->method('findByName')
            ->willReturn(null);

        $this->projectRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $result = $this->SUT->addItemTypes('', [$itemType]);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('created', $result);
        $this->assertArrayHasKey('already_exists', $result);
    }

    /**
     * skipped
     */
    public function testGetProjectById()
    {
        $this->markTestSkipped();
    }

    public function testAssignProjectToUser()
    {
        $projectUuid = Uuid::uuid4();

        $project = new Project();
        $project->setId($projectUuid);

        $user = new User();

        $this->userRepository
            ->method('find')
            ->willReturn($user);

        $this->projectRepository
            ->method('find')
            ->willReturn($project);

        $this->userRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $result = $this->SUT->assignProjectToUser(Uuid::uuid4(), ['']);

        $this->assertInstanceOf(User::class, $result);
        $this->assertInstanceOf(ArrayCollection::class, $user->getProjects());
        $this->assertCount(1, $user->getProjects());
        $this->assertInstanceOf(Project::class, $user->getProjects()[0]);
        $this->assertEquals($projectUuid->toString(), $user->getProjects()[0]->getId()->toString());
    }

    public function testAssignProjectToUserShouldRiseUserNotFoundException()
    {
        $this->expectException(UserNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->assignProjectToUser('', ['']);
    }

    public function testAssignProjectToUserShouldRiseProjectNotFoundException()
    {
        $this->expectException(ProjectNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->projectRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->assignProjectToUser('', ['']);
    }

    public function testRemoveProjectsFromUser()
    {
        $project = new Project();

        $user = new User();
        $user->addProject($project);

        $this->userRepository
            ->method('find')
            ->willReturn($user);

        $this->projectRepository
            ->method('find')
            ->willReturn($project);

        $this->projectRepository
            ->method('find')
            ->willReturn(null);

        $this->userRepository
            ->expects($this->once())
            ->method('persistAndFlush');

        $result = $this->SUT->removeProjectsFromUser(Uuid::uuid4(), ['']);

        $this->assertInstanceOf(User::class, $result);
        $this->assertInstanceOf(ArrayCollection::class, $user->getProjects());
        $this->assertCount(0, $user->getProjects());
    }

    public function testRemoveProjectsFromUserShouldRiseUserNotFoundException()
    {
        $this->expectException(UserNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->removeProjectsFromUser('', ['']);
    }

    public function testDelete()
    {
        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->projectRepository
            ->method('find')
            ->willReturn(new Project());
        $this->projectRepository
            ->expects($this->once())
            ->method('delete');
        $this->projectRepository
            ->expects($this->once())
            ->method('flush');

        $result = $this->SUT->delete(Uuid::uuid4(), Uuid::uuid4());

        $this->assertTrue($result);
    }

    public function testDeleteShouldRiseUserNotFoundException()
    {
        $this->expectException(UserNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->delete(Uuid::uuid4(), Uuid::uuid4());
    }

    public function testDeleteShouldRiseProjectNotFoundException()
    {
        $this->expectException(ProjectNotFoundException::class);

        $this->userRepository
            ->method('find')
            ->willReturn(new User());

        $this->projectRepository
            ->method('find')
            ->willReturn(null);

        $this->SUT->delete(Uuid::uuid4(), Uuid::uuid4());
    }
}