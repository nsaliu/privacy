<?php

namespace App\Tests\Unit\Domain\Translator;


use App\Domain\Entity\Channel;
use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\ItemType;
use App\Domain\Entity\Project;
use App\Domain\Translator\ItemDocumentTranslator;
use App\Infrastructure\ElasticSearch\Entity\Filter\SearchItemDocumentFilter;
use App\Infrastructure\ElasticSearch\Entity\ItemDocument;
use AutoMapperPlus\AutoMapperInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ItemDocumentTranslatorTest extends KernelTestCase
{
    /**
     * @var ItemDocumentTranslator
     */
    private $SUT;

    protected function setUp()
    {
        $kernel = self::bootKernel();
        $mapper = self::$container->get(AutoMapperInterface::class);

        $this->SUT = new ItemDocumentTranslator($mapper);
    }

    public function testFromEntityToDocument()
    {
        $project = new Project();

        $channel = new Channel();
        $channel->setProject($project);

        $itemType = new ItemType();

        $item = new Item();
        $item->setChannel($channel);
        $item->setType($itemType);
        $item->setCreatedAt(new \DateTime());
        $item->setUpdatedAt(new \DateTime());

        $this->assertInstanceOf(ItemDocument::class, $this->SUT->fromEntityToDocument($item));
    }

    public function testFromEntitiesToDocuments()
    {
        $project = new Project();

        $channel = new Channel();
        $channel->setProject($project);

        $itemType = new ItemType();

        $item = new Item();
        $item->setChannel($channel);
        $item->setType($itemType);
        $item->setCreatedAt(new \DateTime());
        $item->setUpdatedAt(new \DateTime());

        $result = $this->SUT->fromEntitiesToDocuments([$item]);

        $this->assertInternalType('array', $result);
        $this->assertCount(1, $result);
        $this->assertInstanceOf(ItemDocument::class, $result[0]);
    }

    public function testFromSearchItemFilterToSearchItemDocumentFilter()
    {
        $searchItemFilter = new SearchItemFilter('', '');

        $this->assertInstanceOf(
            SearchItemDocumentFilter::class,
            $this->SUT->fromSearchItemFilterToSearchItemDocumentFilter($searchItemFilter));
    }

    public function testFromElasticSearchResultToItem()
    {
        $uuid = Uuid::uuid4();

        $itemDocument = new ItemDocument(
            $uuid,
            $uuid,
            $uuid,
            '',
            '',
            true,
            '',
            new \DateTime(),
            new \DateTime()
        );

        $this->assertInstanceOf(Item::class, $this->SUT->fromElasticSearchResultToItem($itemDocument));
    }
}