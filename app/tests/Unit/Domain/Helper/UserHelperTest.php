<?php

namespace App\Tests\Unit\Domain\Helper;

use App\Domain\Entity\Role;
use App\Domain\Entity\User;
use App\Domain\Enum\RoleEnum;
use App\Domain\Exception\PermissionDenied;
use App\Domain\Helper\UserHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserHelperTest extends TestCase
{

    public function testGetUser()
    {
        $user = new User();

        $tokenInterfaceMock = $this->createMock(TokenInterface::class);
        $tokenInterfaceMock
            ->method('getUser')
            ->willReturn($user);

        $tokenStorageInterfaceMock = $this->createMock(TokenStorageInterface::class);
        $tokenStorageInterfaceMock
            ->method('getToken')
            ->willReturn($tokenInterfaceMock);

        $sut = new UserHelper($tokenStorageInterfaceMock);
        $result = $sut->getUser();

        $this->assertInstanceOf(User::class, $result);
    }

    public function testCheckRole()
    {
        $role = new Role();
        $role->setName(RoleEnum::WRITER);

        $user = new User();
        $user->setRole($role);

        $tokenInterfaceMock = $this->createMock(TokenInterface::class);
        $tokenInterfaceMock
            ->method('getUser')
            ->willReturn($user);

        $tokenStorageInterfaceMock = $this->createMock(TokenStorageInterface::class);
        $tokenStorageInterfaceMock
            ->method('getToken')
            ->willReturn($tokenInterfaceMock);

        $sut = new UserHelper($tokenStorageInterfaceMock);
        $result = $sut->checkRole([RoleEnum::WRITER]);

        $this->assertNull($result);
    }

    public function testCheckRoleShouldThrowPermissionDeniedException()
    {
        $this->expectException(PermissionDenied::class);

        $role = new Role();
        $role->setName('wrong_role_name');

        $user = new User();
        $user->setRole($role);

        $tokenInterfaceMock = $this->createMock(TokenInterface::class);
        $tokenInterfaceMock
            ->method('getUser')
            ->willReturn($user);

        $tokenStorageInterfaceMock = $this->createMock(TokenStorageInterface::class);
        $tokenStorageInterfaceMock
            ->method('getToken')
            ->willReturn($tokenInterfaceMock);

        $sut = new UserHelper($tokenStorageInterfaceMock);
        $result = $sut->checkRole([RoleEnum::WRITER]);

        $this->assertNull($result);
    }
}
