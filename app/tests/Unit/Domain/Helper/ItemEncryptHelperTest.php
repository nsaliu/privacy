<?php

namespace App\Tests\Unit\Domain\Helper;

use App\Domain\Helper\ItemEncryptHelper;
use PHPUnit\Framework\TestCase;

class ItemEncryptHelperTest extends TestCase
{
    public function testEncryptInDevEnvShouldNotEncryptValue()
    {
        $env = 'dev';
        $stringToEncrypt = 'test';
        $expected = $stringToEncrypt;

        $sut = new ItemEncryptHelper($env);
        $result = $sut->encrypt($stringToEncrypt);

        $this->assertEquals($expected, $result);
    }

    public function testEncryptInProdEnvShouldEncryptValue()
    {
        $env = 'prod';
        $stringToEncrypt = 'test';
        $expected = md5($stringToEncrypt);

        $sut = new ItemEncryptHelper($env);
        $result = $sut->encrypt($stringToEncrypt);

        $this->assertEquals($expected, $result);
    }
}
