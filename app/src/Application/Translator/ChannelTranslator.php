<?php

namespace App\Application\Translator;

use App\Application\Dto\Input\InputChannelDto;
use App\Application\Dto\Output\ChannelOutputDto;
use App\Application\Dto\Output\ChannelOutputDtoLight;
use App\Domain\Entity\Channel;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;

/**
 * Class ChannelTranslator.
 */
class ChannelTranslator
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * ChannelTranslator constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(
        AutoMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @param Channel $channel
     *
     * @return ChannelOutputDto
     *
     * @throws UnregisteredMappingException
     */
    public function fromEntityToDto(Channel $channel): ChannelOutputDto
    {
        return $this->mapper->map($channel, ChannelOutputDto::class);
    }

    /**
     * @param array|Channel[] $channels
     *
     * @return array|ChannelOutputDto[]
     */
    public function fromEntitiesToDto(array $channels): array
    {
        return $this->mapper->mapMultiple($channels, ChannelOutputDto::class);
    }

    /**
     * @param array|InputChannelDto[] $dtos
     *
     * @return array|Channel[]
     *
     * @throws \Exception
     */
    public function fromInputChannelsDtoToEntities(array $dtos): array
    {
        return $this->mapper->mapMultiple($dtos, Channel::class);
    }

    /**
     * @param array|Channel[] $entities
     *
     * @return array|ChannelOutputDtoLight[]
     */
    public function fromEntitiesToDtoLight(array $entities): array
    {
        return $this->mapper->mapMultiple($entities, ChannelOutputDtoLight::class);
    }
}
