<?php

namespace App\Application\Translator;

use App\Application\Dto\Input\InputItemTypeDto;
use App\Application\Dto\Output\ItemTypeDto;
use App\Domain\Entity\ItemType;
use AutoMapperPlus\AutoMapperInterface;

/**
 * Class ItemTypeTranslator.
 */
class ItemTypeTranslator
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * ItemTypeTranslator constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(
        AutoMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @param array|ItemType[] $itemTypes
     *
     * @return array|ItemTypeDto[]
     */
    public function fromEntitiesToDtos(array $itemTypes): array
    {
        return $this->mapper->mapMultiple($itemTypes, ItemTypeDto::class);
    }

    //endregion from entity to dto

    //region from dto to entity

    /**
     * @param array|InputItemTypeDto[] $dtos
     *
     * @return array|ItemType[]
     *
     * @throws \Exception
     */
    public function fromInputItemTypesDtoToEntity(array $dtos): array
    {
        return $this->mapper->mapMultiple($dtos, ItemType::class);
    }
}
