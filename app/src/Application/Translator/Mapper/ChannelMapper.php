<?php

namespace App\Application\Translator\Mapper;

use App\Application\Dto\Input\InputChannelDto;
use App\Application\Dto\Output\ChannelOutputDto;
use App\Application\Dto\Output\ChannelOutputDtoLight;
use App\Application\Dto\Output\ItemOutputDto;
use App\Domain\Entity\Channel;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use Ramsey\Uuid\Uuid;

/**
 * Class ChannelMapper.
 */
class ChannelMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(Channel::class, ChannelOutputDtoLight::class);
        $config->registerMapping(Channel::class, ChannelOutputDto::class);

        $config->registerMapping(Channel::class, ChannelOutputDto::class)
            ->forMember('items',
                Operation::mapCollectionTo(ItemOutputDto::class)
            )
        ;

        $config->registerMapping(InputChannelDto::class, Channel::class)
            ->forMember('id', function (InputChannelDto $dto) {
                return Uuid::uuid4();
            })
        ;
    }
}
