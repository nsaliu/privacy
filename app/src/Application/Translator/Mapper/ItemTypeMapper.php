<?php

namespace App\Application\Translator\Mapper;

use App\Application\Dto\Input\InputItemTypeDto;
use App\Application\Dto\Output\ItemTypeDto;
use App\Domain\Entity\ItemType;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class ItemTypeMapper.
 */
class ItemTypeMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(ItemType::class, ItemTypeDto::class);

        $config->registerMapping(InputItemTypeDto::class, ItemType::class)
            ->forMember('id', function (InputItemTypeDto $dto) {
                return Uuid::uuid4();
            })
        ;
    }
}
