<?php

namespace App\Application\Translator\Mapper;

use App\Application\Dto\Input\InputProjectDto;
use App\Application\Dto\Output\ProjectOutputDto;
use App\Domain\Entity\Project;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class ProjectMapper.
 */
class ProjectMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(Project::class, ProjectOutputDto::class);

        $config->registerMapping(InputProjectDto::class, Project::class)
            ->forMember('id', function (InputProjectDto $dto) {
                return Uuid::uuid4();
            })
        ;
    }
}
