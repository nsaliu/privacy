<?php

namespace App\Application\Translator\Mapper;

use App\Application\Dto\Input\Filter\ItemFindByFilterDto;
use App\Application\Dto\Input\InputItemDto;
use App\Application\Dto\Input\UpdateItemDto;
use App\Application\Dto\Output\ItemOutputDto;
use App\Domain\Entity\Channel;
use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\ItemType;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\NameConverter\NamingConvention\CamelCaseNamingConvention;
use AutoMapperPlus\NameConverter\NamingConvention\SnakeCaseNamingConvention;
use Ramsey\Uuid\Uuid;

/**
 * Class ItemMapper.
 */
class ItemMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(ItemFindByFilterDto::class, SearchItemFilter::class)
            ->withNamingConventions(
                new SnakeCaseNamingConvention(),
                new CamelCaseNamingConvention()
            )
        ;

        $config->registerMapping(Item::class, ItemOutputDto::class)
            ->forMember('type', function (Item $item) {
                return $item->getType()->getName();
            })
            ->forMember('created_at', function (Item $item) {
                return $item->getCreatedAt()->format('Y-m-d');
            })
            ->forMember('updated_at', function (Item $item) {
                return $item->getUpdatedAt()->format('Y-m-d');
            })
        ;

        $config->registerMapping(InputItemDto::class, Item::class)
            ->forMember('id', function (InputItemDto $dto) {
                return Uuid::uuid4();
            })
            ->forMember('type', function (InputItemDto $dto) {
                $itemtype = new ItemType();
                $itemtype->setId(Uuid::fromString($dto->item_type_id));

                return $itemtype;
            })
        ;

        $config->registerMapping(UpdateItemDto::class, Item::class)
            ->forMember('value', function (UpdateItemDto $dto) {
                return $dto->item_value;
            })
            ->forMember('channel', function (UpdateItemDto $dto) {
                $channel = new Channel();
                $channel->setId(Uuid::fromString($dto->channel_id));

                return $channel;
            })
        ;
    }
}
