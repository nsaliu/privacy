<?php

namespace App\Application\Translator\Mapper;

use App\Application\Dto\Input\CreateUserDto;
use App\Application\Dto\Output\UserOutputDto;
use App\Domain\Entity\Role;
use App\Domain\Entity\User;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class UserMapper.
 */
class UserMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(User::class, UserOutputDto::class);
        $config->registerMapping(UserOutputDto::class, User::class);

        $config->registerMapping(CreateUserDto::class, User::class)
            ->forMember('id', function (CreateUserDto $dto) {
                return Uuid::uuid4();
            })
            ->forMember('role', function (CreateUserDto $dto) {
                $role = new Role();
                $role->setName($dto->role);

                return $role;
            })
        ;
    }
}
