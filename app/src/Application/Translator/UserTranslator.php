<?php

namespace App\Application\Translator;

use App\Application\Dto\Input\CreateUserDto;
use App\Application\Dto\Output\UserOutputDto;
use App\Domain\Entity\User;
use AutoMapperPlus\AutoMapperInterface;
use Exception;

/**
 * Class UserTranslator.
 */
class UserTranslator
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * UserTranslator constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param array|User[] $entities
     *
     * @return array|UserOutputDto[]
     */
    public function fromEntitiesToDto(array $entities): array
    {
        return $this->mapper->mapMultiple($entities, UserOutputDto::class);
    }

    /**
     * @param CreateUserDto $dto
     *
     * @return User
     *
     * @throws Exception
     */
    public function fromCreateUserDtoToEntity(CreateUserDto $dto): User
    {
        return $this->mapper->map($dto, User::class);
    }
}
