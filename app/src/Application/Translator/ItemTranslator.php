<?php

namespace App\Application\Translator;

use App\Application\Dto\Input\Filter\ItemFindByFilterDto;
use App\Application\Dto\Input\InputItemDto;
use App\Application\Dto\Input\UpdateItemDto;
use App\Application\Dto\Output\ItemOutputDto;
use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;

/**
 * Class ItemTranslator.
 */
class ItemTranslator
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * ItemTranslator constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(
        AutoMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @param Item $item
     *
     * @return ItemOutputDto
     *
     * @throws UnregisteredMappingException
     */
    public function fromEntityToDto(Item $item): ItemOutputDto
    {
        return $this->mapper->map($item, ItemOutputDto::class);
    }

    /**
     * @param array|InputItemDto[] $dtos
     *
     * @return array|Item[]
     *
     * @throws \Exception
     */
    public function fromInputItemsDtoToEntities(array $dtos): array
    {
        return $this->mapper->mapMultiple($dtos, Item::class);
    }

    /**
     * @param ItemFindByFilterDto $dto
     *
     * @return SearchItemFilter
     *
     * @throws UnregisteredMappingException
     */
    public function fromItemFindByFilterDtoToSearchItemFilter(ItemFindByFilterDto $dto): SearchItemFilter
    {
        return $this->mapper->map($dto, SearchItemFilter::class);
    }

    /**
     * @param UpdateItemDto $dto
     *
     * @return Item
     *
     * @throws UnregisteredMappingException
     */
    public function fromUpdateItemDtoToItem(UpdateItemDto $dto): Item
    {
        return $this->mapper->map($dto, Item::class);
    }
}
