<?php

namespace App\Application\Translator;

use App\Application\Dto\Input\InputProjectDto;
use App\Application\Dto\Output\ProjectOutputDto;
use App\Domain\Entity\Project;
use AutoMapperPlus\AutoMapperInterface;

/**
 * Class ProjectTranslator.
 */
class ProjectTranslator
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * ProjectTranslator constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(
        AutoMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @param array|Project[] $projects
     *
     * @return array|ProjectOutputDto[]
     */
    public function fromEntitiesToDto(array $projects): array
    {
        return $this->mapper->mapMultiple($projects, ProjectOutputDto::class);
    }

    /**
     * @param InputProjectDto[]|array $dtos
     *
     * @return array|Project[]
     *
     * @throws \Exception
     */
    public function fromInputProjectsDtoToEntities(array $dtos): array
    {
        return $this->mapper->mapMultiple($dtos, Project::class);
    }
}
