<?php

namespace App\Application\Dto\Output;

/**
 * Class ItemTypeDto.
 */
final class ItemTypeDto implements OutputDtoInterface
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;
}
