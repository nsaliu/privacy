<?php

namespace App\Application\Dto\Output;

/**
 * Class UserOutputDto.
 */
final class UserOutputDto implements OutputDtoInterface
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $email;
}
