<?php

namespace App\Application\Dto\Output;

/**
 * Class ProjectOutputDto.
 */
final class ProjectOutputDto implements OutputDtoInterface
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;
}
