<?php

namespace App\Application\Dto\Output;

/**
 * Class ItemOutputDto.
 */
final class ItemOutputDto implements OutputDtoInterface
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $description;

    /**
     * @var bool
     */
    public $accepted;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var string
     */
    public $updated_at;
}
