<?php

namespace App\Application\Dto\Output;

/**
 * Class ChannelOutputDto.
 */
final class ChannelOutputDto implements OutputDtoInterface
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $description;

    /**
     * @var bool
     */
    public $accepted;

    /**
     * @var array
     */
    public $items;
}
