<?php

namespace App\Application\Dto\Output;

/**
 * Class ChannelOutputDtoLight.
 */
final class ChannelOutputDtoLight implements OutputDtoInterface
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $description;

    /**
     * @var bool
     */
    public $accepted;
}
