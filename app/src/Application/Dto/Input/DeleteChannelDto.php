<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DeleteChannelDto.
 */
class DeleteChannelDto implements InputDtoInterface
{
    /**
     * @var string
     */
    public $user_id;

    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $channel_id;
}
