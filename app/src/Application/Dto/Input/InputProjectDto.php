<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InputProjectDto.
 */
final class InputProjectDto implements InputDtoInterface
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;
}
