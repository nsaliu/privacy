<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DeleteItemDto.
 */
class DeleteItemDto implements InputDtoInterface
{
    /**
     * @var string
     */
    public $user_id;

    /**
     * @Assert\NotBlank()
     *
     * @var array
     */
    public $item_values;
}
