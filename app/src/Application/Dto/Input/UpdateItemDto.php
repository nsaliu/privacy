<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UpdateItemDto.
 */
class UpdateItemDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $channel_id;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $item_value;

    /**
     * @var string
     */
    public $description;

    /**
     * @Assert\NotNull()
     *
     * @var bool
     */
    public $accepted;
}
