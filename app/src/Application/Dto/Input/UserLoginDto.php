<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserLoginDto.
 */
class UserLoginDto implements InputDtoInterface
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $email;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $password;
}
