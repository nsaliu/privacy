<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InputChannelDto.
 */
final class InputChannelDto implements InputDtoInterface
{
    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $description;
}
