<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChannelAddItemsDto.
 */
final class ChannelAddItemsDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $channel_id;

    /**
     * @Assert\NotBlank()
     *
     * @var InputItemDto[]
     */
    public $items;
}
