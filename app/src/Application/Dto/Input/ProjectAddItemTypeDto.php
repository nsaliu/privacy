<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectAddItemTypeDto.
 */
final class ProjectAddItemTypeDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $project_id;

    /**
     * @Assert\NotBlank()
     *
     * @var InputItemTypeDto[]
     */
    public $item_types;
}
