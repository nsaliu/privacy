<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DeleteProjectDto.
 */
class DeleteProjectDto implements InputDtoInterface
{
    /**
     * @var string
     */
    public $user_id;

    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $project_id;
}
