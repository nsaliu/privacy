<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AssignProjectToUserDto.
 */
class AssignProjectToUserDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $user_id;

    /**
     * @Assert\NotNull()
     *
     * @var array|string[]
     */
    public $project_ids;
}
