<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RemoveProjectFromUserDto.
 */
class RemoveProjectFromUserDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $user_id;

    /**
     * @Assert\NotBlank()
     *
     * @var array
     */
    public $project_ids;
}
