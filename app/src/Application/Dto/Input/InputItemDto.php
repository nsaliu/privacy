<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InputItemDto.
 */
final class InputItemDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $item_type_id;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $value;

    /**
     * @var string
     */
    public $description;

    /**
     * @Assert\NotBlank()
     *
     * @var bool
     */
    public $accepted;
}
