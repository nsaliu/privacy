<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InputItemTypeDto.
 */
final class InputItemTypeDto
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $id;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $name;
}
