<?php

namespace App\Application\Dto\Input\Filter;

use App\Application\Dto\Input\InputDtoInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ItemFindByFilterDto.
 */
final class ItemFindByFilterDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $channel_id;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $item_value;
}
