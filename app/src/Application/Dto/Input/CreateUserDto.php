<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreateUserDto.
 */
final class CreateUserDto implements InputDtoInterface
{
    /**
     * @Assert\Email()
     *
     * @var string
     */
    public $email;

    /**
     * @Assert\NotCompromisedPassword()
     *
     * @var string
     */
    public $password;

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    public $role;

    /**
     * @Assert\NotBlank()
     *
     * @var bool
     */
    public $active;
}
