<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectAddChannelsDto.
 */
final class ProjectAddChannelsDto implements InputDtoInterface
{
    /**
     * @Assert\Uuid()
     *
     * @var string
     */
    public $project_id;

    /**
     * @Assert\NotBlank()
     *
     * @var InputChannelDto[]
     */
    public $channels;
}
