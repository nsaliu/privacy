<?php

namespace App\Application\Dto\Input;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserAddProjectsDto.
 */
final class UserAddProjectsDto implements InputDtoInterface
{
    /**
     * @Assert\NotBlank()
     *
     * @var InputProjectDto[]|array
     */
    public $projects;
}
