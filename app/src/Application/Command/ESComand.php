<?php

namespace App\Application\Command;

use App\Domain\Translator\ItemDocumentTranslator;
use App\Infrastructure\ElasticSearch\Entity\Filter\SearchItemDocumentFilter;
use App\Infrastructure\ElasticSearch\Entity\ItemDocument;
use App\Infrastructure\ElasticSearch\ESFactory;
use App\Infrastructure\ElasticSearch\Exception\CannotCreateIndexBecauseAlreadyExistsException;
use App\Infrastructure\ElasticSearch\Exception\MappingFileNotFoundException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ESComand.
 */
class ESComand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:es';
    /**
     * @var string|null
     */
    private $name;
    /**
     * @var ESFactory
     */
    private $esManager;
    /**
     * @var ItemDocumentTranslator
     */
    private $itemDocumentTranslator;

    /**
     * ESComand constructor.
     *
     * @param string|null            $name
     * @param ESFactory              $esManager
     * @param ItemDocumentTranslator $itemDocumentTranslator
     */
    public function __construct(
        string $name = null,
        ESFactory $esManager,
        ItemDocumentTranslator $itemDocumentTranslator
    ) {
        parent::__construct($name);
        $this->name = $name;
        $this->esManager = $esManager;
        $this->itemDocumentTranslator = $itemDocumentTranslator;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates the default ES index for this app - Only for testing purpose: this command must be removed.')
            ->addArgument(
                'operation',
                InputArgument::REQUIRED,
                'Operation to do on the index or document. Can be [create, delete, insert]'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     *
     * @throws CannotCreateIndexBecauseAlreadyExistsException
     * @throws MappingFileNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $indexMgr = $this->esManager->getIndexManager();
        $documentManager = $this->esManager->getDocumentManager();

        if ('create' === $input->getArgument('operation')) {
            $output->writeln('Creating index on elasticsearch...');
            $this->deleteIndex();
            $indexMgr->createIndexAndMapping();
            $output->writeln('Index and mapping created successfully!');
        } elseif ('delete' === $input->getArgument('operation')) {
            $output->writeln('Deleting index on elasticsearch...');
            $this->deleteIndex();
            $output->writeln('Index deleted successfully!');
        } elseif ('insert' === $input->getArgument('operation')) {
            $this->deleteIndex();
            $indexMgr->createIndexAndMapping();

            $date = new \DateTime();

            $item_1 = new ItemDocument(
                '43501cf5-6ce8-464a-92e6-0ddde4757985',
                '401afb60-a2c0-4943-a470-5cce1ea9218b',
                '657abc74-bb6e-408b-9c70-66e6deebe63c',
                'email',
                'Item value 1',
                true,
                'Item description',
                $date,
                $date
            );

            $item_2 = new ItemDocument(
                '43501cf5-6ce8-464a-92e6-0ddde4757985',
                '401afb60-a2c0-4943-a470-5cce1ea9218b',
                '8e7dfb27-5fb1-4a2e-b54d-9446295a560b',
                'email',
                'Item value 2',
                true,
                'Item description',
                $date,
                $date
            );

            $output->writeln('Inserting item in elasticsearch...');

            $documentManager->insertMultipleItems([
                $item_1,
                $item_2,
            ]);

            $output->writeln('Item stored in elasticsearch!');
        } elseif ('search' === $input->getArgument('operation')) {
            $filter = new SearchItemDocumentFilter(
                '401afb60-a2c0-4943-a470-5cce1ea9218b',
                'Item value 1'
            );

            $result = $documentManager->findByFilter($filter);

            if (null === $result) {
                $output->writeln('No data retrieved from Elasticsearch.');
                die;
            }

            print_r(
                $this->itemDocumentTranslator->fromElasticSearchResultToItem($result)
            );
        }
    }

    private function deleteIndex()
    {
        try {
            $this->esManager->getIndexManager()->delete();
        } catch (\Exception $ex) {
        }
    }
}
