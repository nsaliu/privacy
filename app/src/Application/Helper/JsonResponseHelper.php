<?php

namespace App\Application\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class JsonResponseService.
 */
class JsonResponseHelper
{
    /**
     * @var JsonResponse
     */
    private $response;

    /**
     * @var string|null
     */
    private $message = null;

    /**
     * @param JsonResponse $response
     *
     * @return JsonResponseHelper
     */
    public function setResponse(JsonResponse $response): self
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return JsonResponseHelper
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param mixed $data
     * @param int   $httpStatusCode
     *
     * @return JsonResponse
     */
    public function writeResponse($data = [], int $httpStatusCode = 200): JsonResponse
    {
        $this->response->setStatusCode($httpStatusCode);
        $this->response->setData(
            $this->buildReturnData($httpStatusCode, $data)
        );

        return $this->response;
    }

    /**
     * @param int   $httpStatusCode
     * @param mixed $data
     *
     * @return array
     */
    private function buildReturnData(int $httpStatusCode, $data = []): array
    {
        $result = [];

        $result['code'] = (int) $httpStatusCode;

        if (null !== $this->message) {
            $result['message'] = strtolower($this->message);
        }

        $result['data'] = $data;
        if (!empty($data)) {
            $result['data'] = $this->decorateData($data);
        }

        return $result;
    }

    /**
     * @param mixed $data
     *
     * @return array
     */
    private function decorateData($data): array
    {
        return [$data];
    }
}
