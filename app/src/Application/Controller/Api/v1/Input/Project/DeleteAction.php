<?php

namespace App\Application\Controller\Api\v1\Input\Project;

use App\Application\Dto\Input\DeleteProjectDto;
use App\Application\Helper\JsonResponseHelper;
use App\Domain\Entity\User;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DeleteAction.
 */
class DeleteAction
{
    /**
     * @var ProjectManagerInterface
     */
    private $projectManager;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * DeleteAction constructor.
     *
     * @param ProjectManagerInterface $projectManager
     * @param JsonResponseHelper      $jsonResponseHelper
     * @param TokenStorageInterface   $tokenStorage
     */
    public function __construct(
        ProjectManagerInterface $projectManager,
        JsonResponseHelper $jsonResponseHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->projectManager = $projectManager;
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param DeleteProjectDto $dto
     *
     * @return JsonResponse
     */
    public function __invoke(DeleteProjectDto $dto): JsonResponse
    {
        if (empty($dto->user_id)) {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $dto->user_id = $user->getId()->toString();
        }

        $this->projectManager->delete($dto->user_id, $dto->project_id);

        return $this->jsonResponseHelper
            ->setMessage('deleted')
            ->setResponse(new JsonResponse())
            ->writeResponse([], 200);
    }
}
