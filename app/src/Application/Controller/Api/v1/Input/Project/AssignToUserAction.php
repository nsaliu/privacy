<?php

namespace App\Application\Controller\Api\v1\Input\Project;

use App\Application\Dto\Input\AssignProjectToUserDto;
use App\Application\Helper\JsonResponseHelper;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AssignToUserAction.
 */
class AssignToUserAction
{
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var ProjectManagerInterface
     */
    private $projectManager;

    /**
     * AssignProjectAction constructor.
     *
     * @param ProjectManagerInterface $projectManager
     * @param JsonResponseHelper      $jsonResponseHelper
     */
    public function __construct(
        ProjectManagerInterface $projectManager,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->projectManager = $projectManager;
    }

    /**
     * @param AssignProjectToUserDto $dto
     *
     * @return JsonResponse
     */
    public function __invoke(AssignProjectToUserDto $dto): JsonResponse
    {
        $this->projectManager->assignProjectToUser(
            $dto->user_id,
            $dto->project_ids
        );

        return $this->jsonResponseHelper
            ->setMessage('associated')
            ->setResponse(new JsonResponse())
            ->writeResponse([], 200);
    }
}
