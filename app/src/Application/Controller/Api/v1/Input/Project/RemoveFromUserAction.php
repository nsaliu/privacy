<?php

namespace App\Application\Controller\Api\v1\Input\Project;

use App\Application\Dto\Input\RemoveProjectFromUserDto;
use App\Application\Helper\JsonResponseHelper;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class RemoveFromUserAction.
 */
class RemoveFromUserAction
{
    /**
     * @var ProjectManagerInterface
     */
    private $projectManager;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;

    /**
     * RemoveFromUserAction constructor.
     *
     * @param ProjectManagerInterface $projectManager
     * @param JsonResponseHelper      $jsonResponseHelper
     */
    public function __construct(
        ProjectManagerInterface $projectManager,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->projectManager = $projectManager;
        $this->jsonResponseHelper = $jsonResponseHelper;
    }

    /**
     * @param RemoveProjectFromUserDto $dto
     *
     * @return JsonResponse
     */
    public function __invoke(RemoveProjectFromUserDto $dto): JsonResponse
    {
        $this->projectManager->removeProjectsFromUser(
            $dto->user_id,
            $dto->project_ids
        );

        return $this->jsonResponseHelper
            ->setMessage('removed')
            ->setResponse(new JsonResponse())
            ->writeResponse([], 200);
    }
}
