<?php

namespace App\Application\Controller\Api\v1\Input\Project;

use App\Application\Dto\Input\ProjectAddItemTypeDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ItemTypeTranslator;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AddItemTypesAction.
 */
class AddItemTypesAction
{
    /**
     * @var ProjectManagerInterface
     */
    private $projectManager;
    /**
     * @var ItemTypeTranslator
     */
    private $itemTypeTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;

    /**
     * AddItemTypesAction constructor.
     *
     * @param ProjectManagerInterface $projectManager
     * @param ItemTypeTranslator      $itemTypeTranslator
     * @param JsonResponseHelper      $jsonResponseHelper
     */
    public function __construct(
        ProjectManagerInterface $projectManager,
        ItemTypeTranslator $itemTypeTranslator,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->projectManager = $projectManager;
        $this->itemTypeTranslator = $itemTypeTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
    }

    /**
     * @param ProjectAddItemTypeDto $dto
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function __invoke(ProjectAddItemTypeDto $dto)
    {
        $result = $this->projectManager->addItemTypes(
            $dto->project_id,
            $this->itemTypeTranslator->fromInputItemTypesDtoToEntity($dto->item_types)
        );

        return $this->jsonResponseHelper->setResponse(new JsonResponse())
            ->setMessage('created')
            ->writeResponse($result, 201);
    }
}
