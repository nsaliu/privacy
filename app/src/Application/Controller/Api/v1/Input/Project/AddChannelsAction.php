<?php

namespace App\Application\Controller\Api\v1\Input\Project;

use App\Application\Dto\Input\ProjectAddChannelsDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ChannelTranslator;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AddChannelsAction.
 */
class AddChannelsAction
{
    /**
     * @var ProjectManagerInterface
     */
    private $projectManager;
    /**
     * @var ChannelTranslator
     */
    private $channelTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AddChannelsAction constructor.
     *
     * @param ProjectManagerInterface $projectManager
     * @param ChannelTranslator       $channelTranslator
     * @param JsonResponseHelper      $jsonResponseHelper
     * @param TokenStorageInterface   $tokenStorage
     */
    public function __construct(
        ProjectManagerInterface $projectManager,
        ChannelTranslator $channelTranslator,
        JsonResponseHelper $jsonResponseHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->projectManager = $projectManager;
        $this->channelTranslator = $channelTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ProjectAddChannelsDto $dto
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function __invoke(ProjectAddChannelsDto $dto): JsonResponse
    {
        $result = $this->projectManager->addChannels(
            $this->tokenStorage->getToken()->getUser(),
            $dto->project_id,
            $this->channelTranslator->fromInputChannelsDtoToEntities($dto->channels)
        );

        return $this->jsonResponseHelper->setResponse(new JsonResponse())
            ->setMessage('created')
            ->writeResponse($result, 201);
    }
}
