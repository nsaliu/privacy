<?php

namespace App\Application\Controller\Api\v1\Input\Channel;

use App\Application\Dto\Input\DeleteChannelDto;
use App\Application\Helper\JsonResponseHelper;
use App\Domain\Entity\User;
use App\Domain\Manager\Interfaces\ChannelManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DeleteAction.
 */
class DeleteAction
{
    /**
     * @var ChannelManagerInterface
     */
    private $channelManager;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * DeleteAction constructor.
     *
     * @param ChannelManagerInterface $channelManager
     * @param TokenStorageInterface   $tokenStorage
     * @param JsonResponseHelper      $jsonResponseHelper
     */
    public function __construct(
        ChannelManagerInterface $channelManager,
        TokenStorageInterface $tokenStorage,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->channelManager = $channelManager;
        $this->tokenStorage = $tokenStorage;
        $this->jsonResponseHelper = $jsonResponseHelper;
    }

    /**
     * @param DeleteChannelDto $dto
     *
     * @return JsonResponse
     */
    public function __invoke(DeleteChannelDto $dto): JsonResponse
    {
        if (empty($dto->user_id)) {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $dto->user_id = $user->getId()->toString();
        }

        $this->channelManager->delete($dto->user_id, $dto->channel_id);

        return $this->jsonResponseHelper
            ->setMessage('deleted')
            ->setResponse(new JsonResponse())
            ->writeResponse([], 200);
    }
}
