<?php

namespace App\Application\Controller\Api\v1\Input\Channel;

use App\Application\Dto\Input\ChannelAddItemsDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ItemTranslator;
use App\Domain\Exception\ItemsAlreadyExistsInChannelException;
use App\Domain\Manager\Interfaces\ChannelManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AddItemsAction.
 */
class AddItemsAction
{
    /**
     * @var ChannelManagerInterface
     */
    private $channelManager;
    /**
     * @var ItemTranslator
     */
    private $itemTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AddItemsAction constructor.
     *
     * @param ChannelManagerInterface $channelManager
     * @param ItemTranslator          $itemTranslator
     * @param JsonResponseHelper      $jsonResponseHelper
     * @param TokenStorageInterface   $tokenStorage
     */
    public function __construct(
        ChannelManagerInterface $channelManager,
        ItemTranslator $itemTranslator,
        JsonResponseHelper $jsonResponseHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->channelManager = $channelManager;
        $this->itemTranslator = $itemTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ChannelAddItemsDto $dto
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function __invoke(ChannelAddItemsDto $dto): JsonResponse
    {
        try {
            $this->channelManager->addItems(
                $this->tokenStorage->getToken()->getUser(),
                $dto->channel_id,
                $this->itemTranslator->fromInputItemsDtoToEntities($dto->items)
            );

            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->setMessage('created')
                ->writeResponse([], 201);
        } catch (ItemsAlreadyExistsInChannelException $ex) {
            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->setMessage('items was created, but these already exists in specified channel and cannot be created twice')
                ->writeResponse($ex->getMessage(), 409);
        }
    }
}
