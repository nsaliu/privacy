<?php

namespace App\Application\Controller\Api\v1\Input\Item;

use App\Application\Dto\Input\DeleteItemDto;
use App\Application\Helper\JsonResponseHelper;
use App\Domain\Entity\User;
use App\Domain\Manager\Interfaces\ItemManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DeleteAction.
 */
class DeleteAction
{
    /**
     * @var ItemManagerInterface
     */
    private $itemManager;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * DeleteAction constructor.
     *
     * @param ItemManagerInterface  $itemManager
     * @param JsonResponseHelper    $jsonResponseHelper
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        ItemManagerInterface $itemManager,
        JsonResponseHelper $jsonResponseHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->itemManager = $itemManager;
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param DeleteItemDto $dto
     *
     * @return JsonResponse
     */
    public function __invoke(DeleteItemDto $dto): JsonResponse
    {
        if (empty($dto->user_id)) {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $dto->user_id = $user->getId()->toString();
        }

        $this->itemManager->delete(
            $dto->user_id,
            $dto->item_values
        );

        return $this->jsonResponseHelper
            ->setMessage('removed')
            ->setResponse(new JsonResponse())
            ->writeResponse([], 200);
    }
}
