<?php

namespace App\Application\Controller\Api\v1\Input\Item;

use App\Application\Dto\Input\UpdateItemDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ItemTranslator;
use App\Domain\Manager\Interfaces\ItemManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class UpdateAction.
 */
class UpdateAction
{
    /**
     * @var ItemManagerInterface
     */
    private $itemManager;
    /**
     * @var ItemTranslator
     */
    private $itemTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;

    /**
     * UpdateAction constructor.
     *
     * @param ItemManagerInterface $itemManager
     * @param ItemTranslator       $itemTranslator
     * @param JsonResponseHelper   $jsonResponseHelper
     */
    public function __construct(
        ItemManagerInterface $itemManager,
        ItemTranslator $itemTranslator,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->itemManager = $itemManager;
        $this->itemTranslator = $itemTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
    }

    /**
     * @param UpdateItemDto $dto
     *
     * @return JsonResponse
     *
     * @throws UnregisteredMappingException
     */
    public function __invoke(UpdateItemDto $dto): JsonResponse
    {
        $this->itemManager->update(
            $this->itemTranslator->fromUpdateItemDtoToItem($dto),
            $dto->channel_id
        );

        return $this->jsonResponseHelper
            ->setResponse(new JsonResponse())
            ->setMessage('updated')
            ->writeResponse([], 200);
    }
}
