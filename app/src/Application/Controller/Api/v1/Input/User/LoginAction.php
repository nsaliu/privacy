<?php

namespace App\Application\Controller\Api\v1\Input\User;

use App\Application\Dto\Input\UserLoginDto;
use App\Application\Helper\JsonResponseHelper;
use App\Domain\Manager\Interfaces\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;

/**
 * Class LoginAction.
 */
class LoginAction
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var JWTEncoderInterface
     */
    private $JWTEncoder;
    /**
     * @var UserCheckerInterface
     */
    private $userChecker;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;

    /**
     * LoginAction constructor.
     *
     * @param UserManagerInterface     $userManager
     * @param PasswordEncoderInterface $passwordEncoder
     * @param JWTEncoderInterface      $JWTEncoder
     * @param UserCheckerInterface     $userChecker
     * @param JsonResponseHelper       $jsonResponseHelper
     */
    public function __construct(
        UserManagerInterface $userManager,
        PasswordEncoderInterface $passwordEncoder,
        JWTEncoderInterface $JWTEncoder,
        UserCheckerInterface $userChecker,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->userManager = $userManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->JWTEncoder = $JWTEncoder;
        $this->userChecker = $userChecker;
        $this->jsonResponseHelper = $jsonResponseHelper;
    }

    /**
     * @param UserLoginDto $loginDto
     *
     * @return JsonResponse
     *
     * @throws JWTEncodeFailureException
     */
    public function __invoke(UserLoginDto $loginDto)
    {
        $user = $this->userManager->findByEmail($loginDto->email);
        if (null === $user) {
            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->setMessage('Wrong username or password')
                ->writeResponse([], 401);
        }

        if (!$user->isActive()) {
            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->setMessage('User is not active')
                ->writeResponse([], 401);
        }

        $this->userChecker->checkPreAuth($user);
        if (!$this->passwordEncoder->isPasswordValid($user->getPassword(), $loginDto->password, $user->getSalt())) {
            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->setMessage('Wrong username or password')
                ->writeResponse([], 401);
        }

        $this->userChecker->checkPostAuth($user);
        $token = $this->JWTEncoder->encode([
            'email' => $user->getEmail(),
            'id' => $user->getId()->toString(),
        ]);

        return $this->jsonResponseHelper->setResponse(new JsonResponse())
            ->setMessage('Welcome!')
            ->writeResponse(['token' => $token], 200);
    }
}
