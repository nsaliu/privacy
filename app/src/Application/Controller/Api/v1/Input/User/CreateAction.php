<?php

namespace App\Application\Controller\Api\v1\Input\User;

use App\Application\Dto\Input\CreateUserDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\UserTranslator;
use App\Domain\Manager\Interfaces\UserManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CreateAction.
 */
class CreateAction
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var UserTranslator
     */
    private $userTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $responseHelper;

    /**
     * CreateAction constructor.
     *
     * @param UserManagerInterface $userManager
     * @param UserTranslator       $userTranslator
     * @param JsonResponseHelper   $responseHelper
     */
    public function __construct(
        UserManagerInterface $userManager,
        UserTranslator $userTranslator,
        JsonResponseHelper $responseHelper
    ) {
        $this->userManager = $userManager;
        $this->userTranslator = $userTranslator;
        $this->responseHelper = $responseHelper;
    }

    /**
     * @param CreateUserDto $dto
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function __invoke(CreateUserDto $dto)
    {
        $this->userManager->create(
            $this->userTranslator->fromCreateUserDtoToEntity($dto)
        );

        return $this->responseHelper->setResponse(new JsonResponse())
            ->setMessage('created')
            ->writeResponse([], 201);
    }
}
