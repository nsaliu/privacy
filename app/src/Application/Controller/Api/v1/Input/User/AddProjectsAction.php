<?php

namespace App\Application\Controller\Api\v1\Input\User;

use App\Application\Dto\Input\UserAddProjectsDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ProjectTranslator;
use App\Domain\Manager\Interfaces\UserManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AddProjectsAction.
 */
class AddProjectsAction
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var ProjectTranslator
     */
    private $projectTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AddProjectsAction constructor.
     *
     * @param UserManagerInterface  $userManager
     * @param ProjectTranslator     $projectTranslator
     * @param JsonResponseHelper    $jsonResponseHelper
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        UserManagerInterface $userManager,
        ProjectTranslator $projectTranslator,
        JsonResponseHelper $jsonResponseHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->userManager = $userManager;
        $this->projectTranslator = $projectTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param UserAddProjectsDto $dto
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function __invoke(UserAddProjectsDto $dto): JsonResponse
    {
        $result = $this->userManager->addProjects(
            $this->tokenStorage->getToken()->getUser(),
            $this->projectTranslator->fromInputProjectsDtoToEntities($dto->projects)
        );

        return $this->jsonResponseHelper->setResponse(new JsonResponse())
            ->setMessage('created')
            ->writeResponse($result, 201);
    }
}
