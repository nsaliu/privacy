<?php

namespace App\Application\Controller\Api\v1\Output\User;

use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\UserTranslator;
use App\Domain\Manager\Interfaces\UserManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ByIdAction.
 */
class ListAction
{
    /**
     * @var JsonResponseHelper
     */
    private $responseService;
    /**
     * @var UserTranslator
     */
    private $userTranslator;
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * UserController constructor.
     *
     * @param UserManagerInterface $userManager
     * @param UserTranslator       $userTranslator
     * @param JsonResponseHelper   $response
     */
    public function __construct(
        UserManagerInterface $userManager,
        UserTranslator $userTranslator,
        JsonResponseHelper $response
    ) {
        $this->responseService = $response;
        $this->userTranslator = $userTranslator;
        $this->userManager = $userManager;
    }

    /**
     * @return JsonResponse
     */
    public function __invoke()
    {
        return $this->responseService
            ->setResponse(new JsonResponse())
            ->writeResponse(
                $this->userTranslator->fromEntitiesToDto(
                    $this->userManager->listUsers()
                ),
                200
            );
    }
}
