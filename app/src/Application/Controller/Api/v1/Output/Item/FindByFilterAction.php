<?php

namespace App\Application\Controller\Api\v1\Output\Item;

use App\Application\Dto\Input\Filter\ItemFindByFilterDto;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ItemTranslator;
use App\Domain\Manager\Interfaces\ItemManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class FindByFilterAction.
 */
class FindByFilterAction
{
    /**
     * @var ItemManagerInterface
     */
    private $itemManager;
    /**
     * @var ItemTranslator
     */
    private $itemTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * FindByFilterAction constructor.
     *
     * @param ItemManagerInterface  $itemManager
     * @param ItemTranslator        $itemTranslator
     * @param JsonResponseHelper    $jsonResponseHelper
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        ItemManagerInterface $itemManager,
        ItemTranslator $itemTranslator,
        JsonResponseHelper $jsonResponseHelper,
        TokenStorageInterface $tokenStorage
    ) {
        $this->itemManager = $itemManager;
        $this->itemTranslator = $itemTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param ItemFindByFilterDto $filter
     *
     * @return JsonResponse
     *
     * @throws UnregisteredMappingException
     */
    public function __invoke(ItemFindByFilterDto $filter): JsonResponse
    {
        $result = $this->itemManager->findByFilter(
            $this->tokenStorage->getToken()->getUser(),
            $this->itemTranslator->fromItemFindByFilterDtoToSearchItemFilter($filter)
        );

        if (null !== $result) {
            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->writeResponse(
                    $this->itemTranslator->fromEntityToDto($result),
                    200
                );
        } else {
            return $this->jsonResponseHelper->setResponse(new JsonResponse())
                ->writeResponse(
                    [],
                    404
                );
        }
    }
}
