<?php

namespace App\Application\Controller\Api\v1\Output\Item;

use App\Domain\Manager\Interfaces\ItemManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ItemTranslator;

/**
 * Class ListByChannelIdAction.
 */
class ListByChannelIdAction
{
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseService;
    /**
     * @var ItemTranslator
     */
    private $itemTranslator;
    /**
     * @var ItemManagerInterface
     */
    private $itemManager;

    /**
     * ListByChannelIdAction constructor.
     *
     * @param ItemManagerInterface $itemManager
     * @param ItemTranslator       $itemTranslator
     * @param JsonResponseHelper   $jsonResponseService
     */
    public function __construct(
        ItemManagerInterface $itemManager,
        ItemTranslator $itemTranslator,
        JsonResponseHelper $jsonResponseService
    ) {
        $this->jsonResponseService = $jsonResponseService;
        $this->itemTranslator = $itemTranslator;
        $this->itemManager = $itemManager;
    }

    /**
     * @param string $channelId
     *
     * @return JsonResponse
     */
    public function __invoke(string $channelId): JsonResponse
    {
        // todo
    }
}
