<?php

namespace App\Application\Controller\Api\v1\Output\Project;

use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ProjectTranslator;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ListByUserAction.
 */
class ListByUserAction
{
    /**
     * @var JsonResponseHelper
     */
    private $responseService;
    /**
     * @var ProjectTranslator
     */
    private $projectTranslator;
    /**
     * @var ProjectManagerInterface
     */
    private $projectManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * ListByUserIdAction constructor.
     *
     * @param ProjectManagerInterface $projectManager
     * @param ProjectTranslator       $projectTranslator
     * @param JsonResponseHelper      $responseService
     * @param TokenStorageInterface   $tokenStorage
     */
    public function __construct(
        ProjectManagerInterface $projectManager,
        ProjectTranslator $projectTranslator,
        JsonResponseHelper $responseService,
        TokenStorageInterface $tokenStorage
    ) {
        $this->responseService = $responseService;
        $this->projectTranslator = $projectTranslator;
        $this->projectManager = $projectManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param string $userId
     *
     * @return JsonResponse
     */
    public function __invoke(string $userId): JsonResponse
    {
        return $this->responseService
            ->setResponse(new JsonResponse())
            ->writeResponse(
                $this->projectTranslator->fromEntitiesToDto(
                    $this->projectManager->findByUser(
                        $userId
                    )
                ),
                200
            );
    }
}
