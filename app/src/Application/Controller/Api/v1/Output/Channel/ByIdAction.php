<?php

namespace App\Application\Controller\Api\v1\Output\Channel;

use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ChannelTranslator;
use App\Domain\Manager\Interfaces\ChannelManagerInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ByIdAction.
 */
class ByIdAction
{
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseService;
    /**
     * @var ChannelTranslator
     */
    private $channelTranslator;
    /**
     * @var ChannelManagerInterface
     */
    private $channelManager;

    /**
     * ByIdAction constructor.
     *
     * @param ChannelManagerInterface $channelManager
     * @param ChannelTranslator       $channelTranslator
     * @param JsonResponseHelper      $jsonResponseService
     */
    public function __construct(
        ChannelManagerInterface $channelManager,
        ChannelTranslator $channelTranslator,
        JsonResponseHelper $jsonResponseService
    ) {
        $this->jsonResponseService = $jsonResponseService;
        $this->channelTranslator = $channelTranslator;
        $this->channelManager = $channelManager;
    }

    /**
     * @param string $channelId
     *
     * @return JsonResponse
     *
     * @throws UnregisteredMappingException
     */
    public function __invoke(string $channelId): JsonResponse
    {
        return $this->jsonResponseService
            ->setResponse(new JsonResponse())
            ->writeResponse(
                $this->channelTranslator->fromEntityToDto(
                    $this->channelManager->findById($channelId)
                ),
                200
            );
    }
}
