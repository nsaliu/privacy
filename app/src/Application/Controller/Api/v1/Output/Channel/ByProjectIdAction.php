<?php

namespace App\Application\Controller\Api\v1\Output\Channel;

use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ChannelTranslator;
use App\Domain\Manager\Interfaces\ChannelManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ByProjectIdAction.
 */
class ByProjectIdAction
{
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseService;
    /**
     * @var ChannelTranslator
     */
    private $channelTranslator;
    /**
     * @var ChannelManagerInterface
     */
    private $channelManager;

    /**
     * ByProjectIdAction constructor.
     *
     * @param ChannelManagerInterface $channelManager
     * @param ChannelTranslator       $channelTranslator
     * @param JsonResponseHelper      $jsonResponseService
     */
    public function __construct(
        ChannelManagerInterface $channelManager,
        ChannelTranslator $channelTranslator,
        JsonResponseHelper $jsonResponseService
    ) {
        $this->jsonResponseService = $jsonResponseService;
        $this->channelTranslator = $channelTranslator;
        $this->channelManager = $channelManager;
    }

    /**
     * @param string $projectId
     *
     * @return JsonResponse
     */
    public function __invoke(string $projectId): JsonResponse
    {
        return $this->jsonResponseService
            ->setResponse(new JsonResponse())
            ->writeResponse(
                $this->channelTranslator->fromEntitiesToDtoLight(
                    $this->channelManager->findChannelsByProjectId($projectId)
                ),
                200
            );
    }
}
