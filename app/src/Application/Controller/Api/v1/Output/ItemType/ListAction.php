<?php

namespace App\Application\Controller\Api\v1\Output\ItemType;

use App\Application\Helper\JsonResponseHelper;
use App\Application\Translator\ItemTypeTranslator;
use App\Domain\Manager\Interfaces\ItemTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ListAction.
 */
class ListAction
{
    /**
     * @var ItemTypeManagerInterface
     */
    private $itemTypeManager;
    /**
     * @var ItemTypeTranslator
     */
    private $itemTypeTranslator;
    /**
     * @var JsonResponseHelper
     */
    private $jsonResponseHelper;

    /**
     * ListAction constructor.
     *
     * @param ItemTypeManagerInterface $itemTypeManager
     * @param ItemTypeTranslator       $itemTypeTranslator
     * @param JsonResponseHelper       $jsonResponseHelper
     */
    public function __construct(
        ItemTypeManagerInterface $itemTypeManager,
        ItemTypeTranslator $itemTypeTranslator,
        JsonResponseHelper $jsonResponseHelper
    ) {
        $this->itemTypeManager = $itemTypeManager;
        $this->itemTypeTranslator = $itemTypeTranslator;
        $this->jsonResponseHelper = $jsonResponseHelper;
    }

    /**
     * @param string $projectId
     *
     * @return JsonResponse
     */
    public function __invoke(string $projectId): JsonResponse
    {
        return $this->jsonResponseHelper->setResponse(new JsonResponse())
            ->writeResponse(
                $this->itemTypeTranslator->fromEntitiesToDtos(
                    $this->itemTypeManager->findAll($projectId)
                ),
                200
            );
    }
}
