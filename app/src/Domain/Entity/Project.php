<?php

namespace App\Domain\Entity;

use App\Domain\Entity\Traits\OwnableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Project.
 *
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\ProjectRepository")
 */
class Project extends PersistibleEntity
{
    use TimestampableEntity;
    use OwnableEntity;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Channel", mappedBy="project", cascade={"persist"}, orphanRemoval=true)
     *
     * @var Collection|Channel[]
     */
    private $channels;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="projects")
     *
     * @var Collection|User[]
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="ItemType", cascade={"persist"}, fetch="EAGER", mappedBy="project", orphanRemoval=true)
     *
     * @var Collection|ItemType[]
     */
    private $itemsType;

    /**
     * Project constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->channels = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->itemsType = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Channel[]|Collection
     */
    public function getChannels(): Collection
    {
        return $this->channels;
    }

    /**
     * @param Channel $channel
     */
    public function addChannel(Channel $channel): void
    {
        if (!$this->channels->contains($channel)) {
            $channel->setProject($this);
            $this->channels->add($channel);
        }
    }

    /**
     * @param Channel $channel
     */
    public function removeChannel(Channel $channel): void
    {
        if ($this->channels->contains($channel)) {
            $channel->setProject(null);
            $this->channels->removeElement($channel);
        }
    }

    /**
     * @return User[]|Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param User $user
     */
    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    /**
     * @param User $user
     */
    public function removeUser(User $user): void
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
    }

    /**
     * @return ItemType[]|Collection
     */
    public function getItemsType(): Collection
    {
        return $this->itemsType;
    }

    /**
     * @param ItemType $itemType
     */
    public function addItemsType(ItemType $itemType): void
    {
        if (!$this->itemsType->contains($itemType)) {
            $itemType->setProject($this);
            $this->itemsType->add($itemType);
        }
    }

    /**
     * @param ItemType $itemType
     */
    public function removeItemsType(ItemType $itemType): void
    {
        if ($this->itemsType->contains($itemType)) {
            $itemType->setProject(null);
            $this->itemsType->removeElement($itemType);
        }
    }
}
