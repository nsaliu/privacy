<?php

namespace App\Domain\Entity;

use App\Domain\Entity\Traits\OwnableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Channel.
 *
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\ChannelRepository")
 */
class Channel extends PersistibleEntity
{
    use TimestampableEntity;
    use OwnableEntity;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="`type`", type="string", length=255)
     *
     * @var string
     */
    private $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="channel", cascade={"persist"}, orphanRemoval=true)
     *
     * @var Collection|Item[]
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="channels")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var Project
     */
    private $project;

    /**
     * Channel constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->items = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Item[]|Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item): void
    {
        if (!$this->items->contains($item)) {
            $item->setChannel($this);
            $this->items->add($item);
        }
    }

    /**
     * @param Item $item
     */
    public function removeItem(Item $item): void
    {
        if ($this->items->contains($this)) {
            $item->setChannel(null);
            $this->items->removeElement($item);
        }
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project): void
    {
        $this->project = $project;
    }
}
