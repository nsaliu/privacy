<?php

namespace App\Domain\Entity\Filter;

/**
 * Class FindItemFilter.
 */
final class SearchItemFilter
{
    /**
     * @var string
     */
    private $channelId;

    /**
     * @var string
     */
    private $itemValue;

    /**
     * FindItemFilter constructor.
     *
     * @param string $channelId
     * @param string $itemValue
     */
    public function __construct(
        string $channelId,
        string $itemValue
    ) {
        $this->channelId = $channelId;
        $this->itemValue = $itemValue;
    }

    /**
     * @return string
     */
    public function getChannelId(): string
    {
        return $this->channelId;
    }

    /**
     * @return string
     */
    public function getItemValue(): string
    {
        return $this->itemValue;
    }
}
