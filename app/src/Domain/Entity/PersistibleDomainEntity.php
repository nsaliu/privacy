<?php

namespace App\Domain\Entity;

use Ramsey\Uuid\UuidInterface;

/**
 * Interfaces PersistibleDomainEntity.
 */
interface PersistibleDomainEntity
{
    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @param UuidInterface $uuid
     */
    public function setId(UuidInterface $uuid): void;
}
