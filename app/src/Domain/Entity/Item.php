<?php

namespace App\Domain\Entity;

use App\Domain\Entity\Traits\OwnableEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Item.
 *
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\ItemRepository")
 */
class Item extends PersistibleEntity
{
    use TimestampableEntity;
    use OwnableEntity;

    /**
     * @ORM\ManyToOne(targetEntity="ItemType", fetch="EAGER")
     *
     * @var ItemType
     */
    private $type;

    /**
     * @ORM\Column(name="`value`", type="string", length=500)
     *
     * @var string
     */
    private $value;

    /**
     * @ORM\Column(type="text")
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $accepted;

    /**
     * @ORM\ManyToOne(targetEntity="Channel", inversedBy="items")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var Channel
     */
    private $channel;

    /**
     * Item constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->accepted = false;
    }

    /**
     * @return ItemType
     */
    public function getType(): ItemType
    {
        return $this->type;
    }

    /**
     * @param ItemType $type
     */
    public function setType(ItemType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isAccepted(): bool
    {
        return $this->accepted;
    }

    /**
     * @param bool $accepted
     */
    public function setAccepted(bool $accepted): void
    {
        $this->accepted = $accepted;
    }

    /**
     * @return Channel
     */
    public function getChannel(): Channel
    {
        return $this->channel;
    }

    /**
     * @param Channel $channel
     */
    public function setChannel(Channel $channel): void
    {
        $this->channel = $channel;
    }
}
