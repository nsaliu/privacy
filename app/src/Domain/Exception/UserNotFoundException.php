<?php

namespace App\Domain\Exception;

/**
 * Class UserNotFoundException.
 */
class UserNotFoundException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'User not found';
}
