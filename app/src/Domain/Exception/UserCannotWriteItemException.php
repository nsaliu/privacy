<?php

namespace App\Domain\Exception;

/**
 * Class UserCannotWriteItemException.
 */
class UserCannotWriteItemException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'User have only read permission';
}
