<?php

namespace App\Domain\Exception;

/**
 * Class UserAlreadyExistsException.
 */
class UserAlreadyExistsException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'User already exists';
}
