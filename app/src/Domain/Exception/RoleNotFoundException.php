<?php

namespace App\Domain\Exception;

/**
 * Class RoleNotFoundException.
 */
class RoleNotFoundException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Role not found';
}
