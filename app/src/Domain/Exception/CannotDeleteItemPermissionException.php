<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class CannotDeleteItemException.
 */
class CannotDeleteItemPermissionException extends \Exception
{
    /**
     * CannotDeleteItemException constructor.
     *
     * @param string         $itemValue
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($itemValue = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Can not delete Item with value: '.$itemValue, $code, $previous);
    }
}
