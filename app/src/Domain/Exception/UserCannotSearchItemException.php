<?php

namespace App\Domain\Exception;

/**
 * Class UserCannotSearchItemException.
 */
class UserCannotSearchItemException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'User cannot search item';
}
