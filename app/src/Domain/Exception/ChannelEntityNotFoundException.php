<?php

namespace App\Domain\Exception;

/**
 * Class ChannelEntityNotFoundException.
 */
class ChannelEntityNotFoundException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Channel entity not found';
}
