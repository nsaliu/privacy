<?php

namespace App\Domain\Exception;

/**
 * Class UserIsNotActiveExcption.
 */
class UserIsNotActiveExcption extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'User is not active';
}
