<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class ItemsAlreadyExistsInChannelException.
 */
class ItemsAlreadyExistsInChannelException extends \Exception
{
    /**
     * ItemsAlreadyExistsInChannelException constructor.
     *
     * @param string         $channelId
     * @param array          $itemValues
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $channelId, array $itemValues, $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            sprintf(
                'items was created, but these [%s] already exists in channel [%s] and cannot be created twice',
                join(', ', $itemValues),
                $channelId
            ),
            $code,
            $previous);
    }
}
