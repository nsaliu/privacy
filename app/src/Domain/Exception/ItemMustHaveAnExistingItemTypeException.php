<?php

namespace App\Domain\Exception;

/**
 * Class ItemMustHaveAnExistingItemTypeException.
 */
class ItemMustHaveAnExistingItemTypeException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'An Item must reference to an existing ItemType: given itemType ID was not found';
}
