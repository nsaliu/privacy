<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class ProjectEntityNotFoundException.
 */
class ProjectNotFoundException extends \Exception
{
    /**
     * ProjectNotFoundException constructor.
     *
     * @param string         $projectId
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($projectId = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Project entity not found with id [%s]',
                $projectId
            ),
            $code,
            $previous
        );
    }
}
