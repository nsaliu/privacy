<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class CannotDeleteChannelPermissionException.
 */
class CannotDeleteChannelPermissionException extends \Exception
{
    /**
     * CannotDeleteChannelPermissionException constructor.
     *
     * @param string         $channelId
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $channelId, $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            sprintf('Channel with id [%s] cannot be deleted',
                $channelId
            ),
            $code,
            $previous
        );
    }
}
