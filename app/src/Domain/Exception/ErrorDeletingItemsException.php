<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class ErrorDeletingItemsException.
 */
class ErrorDeletingItemsException extends \Exception
{
    /**
     * ErrorDeletingItemsException constructor.
     *
     * @param array          $itemIds
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(array $itemIds = [], $code = 0, Throwable $previous = null)
    {
        $itemIds = join(', ', $itemIds);
        parent::__construct('Error deleting this Item IDs: '.$itemIds, $code, $previous);
    }
}
