<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class PermissionDenied.
 */
class PermissionDenied extends \Exception
{
    /**
     * PermissionDenied constructor.
     *
     * @param string         $role
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $role, $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            sprintf("Your role [%s] can't complete this action",
                $role
            ),
            $code,
            $previous
        );
    }
}
