<?php

namespace App\Domain\Exception;

use Throwable;

/**
 * Class ItemNotFoundException.
 */
class ItemNotFoundException extends \Exception
{
    /**
     * ItemNotFoundException constructor.
     *
     * @param string         $itemValue
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($itemValue = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Item not exists with value: '.$itemValue, $code, $previous);
    }
}
