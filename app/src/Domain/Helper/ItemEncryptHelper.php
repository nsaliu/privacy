<?php

namespace App\Domain\Helper;

/**
 * Class ItemEncryptHelper.
 */
class ItemEncryptHelper
{
    /**
     * @var string
     */
    private $env;

    /**
     * ItemEncryptHelper constructor.
     *
     * @param string $env
     */
    public function __construct(string $env)
    {
        $this->env = $env;
    }

    /**
     * @param string $itemValue
     *
     * @return string
     */
    public function encrypt(string $itemValue): string
    {
        if ('dev' === $this->env) {
            return $itemValue;
        }

        return md5($itemValue);
    }
}
