<?php

namespace App\Domain\Helper;

use App\Domain\Entity\User;
use App\Domain\Enum\RoleEnum;
use App\Domain\Exception\PermissionDenied;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UserHelper.
 */
class UserHelper
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserHelper constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;

        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param array $roles
     *
     * @throws PermissionDenied
     */
    public function checkRole(array $roles): void
    {
        $roles[] = RoleEnum::ADMIN;
        $userRole = $this->user->getRole()->getName();

        if (!in_array($userRole, $roles)) {
            throw new PermissionDenied($userRole);
        }
    }
}
