<?php

namespace App\Domain\Translator;

use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Infrastructure\ElasticSearch\Entity\Filter\SearchItemDocumentFilter;
use App\Infrastructure\ElasticSearch\Entity\ItemDocument;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;

/**
 * Class ItemDocumentTranslator.
 */
class ItemDocumentTranslator
{
    /**
     * @var AutoMapperInterface
     */
    private $mapper;

    /**
     * ItemDocumentTranslator constructor.
     *
     * @param AutoMapperInterface $mapper
     */
    public function __construct(
        AutoMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @param Item $item
     *
     * @return ItemDocument
     *
     * @throws UnregisteredMappingException
     */
    public function fromEntityToDocument(Item $item): ItemDocument
    {
        return $this->mapper->map($item, ItemDocument::class);
    }

    /**
     * @param Item[]|array $items
     *
     * @return array
     */
    public function fromEntitiesToDocuments(array $items): array
    {
        return $this->mapper->mapMultiple($items, ItemDocument::class);
    }

    /**
     * @param SearchItemFilter $filter
     *
     * @return SearchItemDocumentFilter
     *
     * @throws UnregisteredMappingException
     */
    public function fromSearchItemFilterToSearchItemDocumentFilter(SearchItemFilter $filter): SearchItemDocumentFilter
    {
        return $this->mapper->map($filter, SearchItemDocumentFilter::class);
    }

    /**
     * @param ItemDocument $item
     *
     * @return Item
     *
     * @throws \Exception
     */
    public function fromElasticSearchResultToItem(ItemDocument $item): Item
    {
        return $this->mapper->map($item, Item::class);
    }
}
