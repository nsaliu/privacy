<?php

namespace App\Domain\Translator\Mapper;

use App\Domain\Entity\Filter\SearchItemFilter;
use App\Infrastructure\ElasticSearch\Entity\Filter\SearchItemDocumentFilter;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;

/**
 * Class SearchItemDocumentFilterMapper.
 */
class SearchItemDocumentFilterMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(SearchItemFilter::class, SearchItemDocumentFilter::class);
    }
}
