<?php

namespace App\Domain\Translator\Mapper;

use App\Domain\Entity\Channel;
use App\Domain\Entity\Item;
use App\Domain\Entity\ItemType;
use App\Domain\Entity\Project;
use App\Infrastructure\ElasticSearch\Entity\ItemDocument;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class ItemDocumentMapper.
 */
class ItemDocumentMapper implements AutoMapperConfiguratorInterface
{
    /**
     * @param AutoMapperConfigInterface $config
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(Item::class, ItemDocument::class)
            ->forMember('projectId', function (Item $item) {
                return $item->getChannel()->getProject()->getId();
            })
            ->forMember('channelId', function (Item $item) {
                return $item->getChannel()->getId();
            })
            ->forMember('type', function (Item $item) {
                return $item->getType()->getName();
            })
            ->forMember('createdAt', function (Item $item) {
                return \DateTimeImmutable::createFromMutable($item->getCreatedAt());
            })
            ->forMember('updatedAt', function (Item $item) {
                return \DateTimeImmutable::createFromMutable($item->getUpdatedAt());
            });

        $config->registerMapping(ItemDocument::class, Item::class)
            ->forMember('id', function (ItemDocument $item) {
                return Uuid::fromString($item->getId());
            })
            ->forMember('project', function (ItemDocument $item) {
                $project = new Project();
                $project->setId(Uuid::fromString($item->getProjectId()));

                return $project;
            })
            ->forMember('channel', function (ItemDocument $item) {
                $channel = new Channel();
                $channel->setId(Uuid::fromString($item->getChannelId()));

                return $channel;
            })
            ->forMember('type', function (ItemDocument $item) {
                $itemType = new ItemType();
                $itemType->setName($item->getType());

                return $itemType;
            });
    }
}
