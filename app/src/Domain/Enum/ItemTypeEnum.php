<?php

namespace App\Domain\Enum;

/**
 * Class ItemTypeEnum.
 */
final class ItemTypeEnum
{
    const EMAIL = 'email';
    const PHONE = 'phone';
}
