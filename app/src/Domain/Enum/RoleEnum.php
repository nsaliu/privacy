<?php

namespace App\Domain\Enum;

/**
 * Class RoleEnum.
 */
final class RoleEnum
{
    public const READER = 'reader';
    public const WRITER = 'writer';
    public const ADMIN = 'reader_and_writer';
}
