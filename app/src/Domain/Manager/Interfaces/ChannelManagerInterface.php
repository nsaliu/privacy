<?php

namespace App\Domain\Manager\Interfaces;

use App\Domain\Entity\Channel;
use App\Domain\Entity\User;
use App\Domain\Exception\ChannelEntityNotFoundException;
use App\Domain\Exception\ItemMustHaveAnExistingItemTypeException;
use App\Domain\Exception\ItemsAlreadyExistsInChannelException;
use App\Domain\Exception\UserCannotWriteItemException;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Interfaces ChannelManagerInterface.
 */
interface ChannelManagerInterface
{
    /**
     * @param string $projectId
     *
     * @return array
     */
    public function findChannelsByProjectId(string $projectId): array;

    /**
     * @param string $channelId
     *
     * @return Channel
     */
    public function findById(string $channelId): Channel;

    /**
     * @param User   $user
     * @param string $channelId
     * @param array  $items
     *
     * @return Channel
     *
     * @throws ChannelEntityNotFoundException
     * @throws ItemMustHaveAnExistingItemTypeException
     * @throws ItemsAlreadyExistsInChannelException
     * @throws NonUniqueResultException
     * @throws UserCannotWriteItemException
     */
    public function addItems(User $user, string $channelId, array $items): Channel;

    /**
     * @param string $userId
     * @param string $channelId
     *
     * @return bool
     */
    public function delete(string $userId, string $channelId): bool;
}
