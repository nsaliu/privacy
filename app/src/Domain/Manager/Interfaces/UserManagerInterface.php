<?php

namespace App\Domain\Manager\Interfaces;

use App\Domain\Entity\Project;
use App\Domain\Entity\User;

/**
 * Interfaces UserManagerInterface.
 */
interface UserManagerInterface
{
    /**
     * @return array
     */
    public function listUsers(): array;

    /**
     * @param User $user
     *
     * @return User
     */
    public function create(User $user): User;

    /**
     * @param User            $user
     * @param array|Project[] $projects
     *
     * @return array
     */
    public function addProjects(User $user, array $projects): array;

    /**
     * @param string $email
     *
     * @return User|null
     */
    public function findByEmail(string $email): ?User;

    /**
     * @param string $id
     *
     * @return User|null
     */
    public function findById(string $id): ?User;
}
