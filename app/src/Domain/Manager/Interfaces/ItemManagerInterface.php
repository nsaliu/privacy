<?php

namespace App\Domain\Manager\Interfaces;

use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\User;

/**
 * Interfaces ItemManagerInterface.
 */
interface ItemManagerInterface
{
    /**
     * @param User             $user
     * @param SearchItemFilter $filter
     *
     * @return Item|null
     */
    public function findByFilter(User $user, SearchItemFilter $filter): ?Item;

    /**
     * @param string $userId
     * @param array  $itemValues
     *
     * @return bool
     */
    public function delete(string $userId, array $itemValues): bool;

    /**
     * @param Item   $item
     * @param string $channelId
     *
     * @return Item
     */
    public function update(Item $item, string $channelId): Item;
}
