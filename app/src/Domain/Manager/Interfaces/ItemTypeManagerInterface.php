<?php

namespace App\Domain\Manager\Interfaces;

/**
 * Interfaces ItemTypeManagerInterface.
 */
interface ItemTypeManagerInterface
{
    /**
     * @param string $projectId
     *
     * @return array
     */
    public function findAll(string $projectId): array;
}
