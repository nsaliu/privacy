<?php

namespace App\Domain\Manager\Interfaces;

use App\Domain\Entity\User;
use App\Domain\Exception\ProjectNotFoundException;

/**
 * Interfaces ProjectManagerInterface.
 */
interface ProjectManagerInterface
{
    /**
     * @param string $userId
     *
     * @return array
     */
    public function findByUser(string $userId): array;

    /**
     * @param User   $user
     * @param string $projectId
     * @param array  $channels
     *
     * @return array
     *
     * @throws ProjectNotFoundException
     */
    public function addChannels(User $user, string $projectId, array $channels): array;

    /**
     * @param string $projectId
     * @param array  $itemTypes
     *
     * @return array
     */
    public function addItemTypes(string $projectId, array $itemTypes): array;

    /**
     * @param string $userId
     * @param array  $projectIds
     *
     * @return User
     */
    public function assignProjectToUser(string $userId, array $projectIds): User;

    /**
     * @param string $userId
     * @param array  $projectIds
     *
     * @return User
     */
    public function removeProjectsFromUser(string $userId, array $projectIds): User;

    /**
     * @param string $userId
     * @param string $projectId
     *
     * @return bool
     */
    public function delete(string $userId, string $projectId): bool;
}
