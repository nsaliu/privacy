<?php

namespace App\Domain\Manager;

use App\Domain\Entity\Channel;
use App\Domain\Entity\Item;
use App\Domain\Entity\User;
use App\Domain\Exception\ChannelEntityNotFoundException;
use App\Domain\Exception\ItemMustHaveAnExistingItemTypeException;
use App\Domain\Exception\ItemsAlreadyExistsInChannelException;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Helper\ItemEncryptHelper;
use App\Domain\Manager\Interfaces\ChannelManagerInterface;
use App\Domain\Translator\ItemDocumentTranslator;
use App\Infrastructure\ElasticSearch\ESFactory;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use App\Infrastructure\Repository\ItemRepository;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class channelManager.
 */
class ChannelManager implements ChannelManagerInterface
{
    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;
    /**
     * @var ItemTypeRepositoryInterface
     */
    private $itemTypeRepository;
    /**
     * @var ESFactory
     */
    private $esManager;
    /**
     * @var ItemDocumentTranslator
     */
    private $itemDocumentTranslator;
    /**
     * @var ItemRepository
     */
    private $itemRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;
    /**
     * @var ItemEncryptHelper
     */
    private $encryptHelper;

    /**
     * channelManager constructor.
     *
     * @param ChannelRepositoryInterface  $channelRepository
     * @param ItemTypeRepositoryInterface $itemTypeRepository
     * @param ESFactory                   $esManager
     * @param ItemDocumentTranslator      $itemDocumentTranslator
     * @param ItemRepository              $itemRepository
     * @param UserRepositoryInterface     $userRepository
     * @param ProjectRepositoryInterface  $projectRepository
     * @param ItemEncryptHelper           $encryptHelper
     */
    public function __construct(
        ChannelRepositoryInterface $channelRepository,
        ItemTypeRepositoryInterface $itemTypeRepository,
        ESFactory $esManager,
        ItemDocumentTranslator $itemDocumentTranslator,
        ItemRepository $itemRepository,
        UserRepositoryInterface $userRepository,
        ProjectRepositoryInterface $projectRepository,
        ItemEncryptHelper $encryptHelper
    ) {
        $this->channelRepository = $channelRepository;
        $this->itemTypeRepository = $itemTypeRepository;
        $this->esManager = $esManager;
        $this->itemDocumentTranslator = $itemDocumentTranslator;
        $this->itemRepository = $itemRepository;
        $this->userRepository = $userRepository;
        $this->projectRepository = $projectRepository;
        $this->encryptHelper = $encryptHelper;
    }

    /**
     * @param string $projectId
     *
     * @return Channel[]|array
     */
    public function findChannelsByProjectId(string $projectId): array
    {
        return $this->channelRepository->findByProjectId($projectId);
    }

    /**
     * @param string $channelId
     *
     * @return Channel
     */
    public function findById(string $channelId): Channel
    {
        return $this->channelRepository->find($channelId);
    }

    /**
     * @param User         $user
     * @param string       $channelId
     * @param array|Item[] $items
     *
     * @return Channel
     *
     * @throws ChannelEntityNotFoundException
     * @throws ItemMustHaveAnExistingItemTypeException
     * @throws ItemsAlreadyExistsInChannelException
     * @throws NonUniqueResultException
     */
    public function addItems(User $user, string $channelId, array $items): Channel
    {
        $channel = $this->channelRepository->find($channelId);
        if (empty($channel)) {
            throw new ChannelEntityNotFoundException();
        }

        $notCreatedItems = [];
        $createdItems = [];
        foreach ($items as $item) {
            $itemFromDb = $this->itemRepository->findByChannelIdAndValue($channelId, $item->getValue());
            if (null !== $itemFromDb) {
                $notCreatedItems[] = $itemFromDb->getValue();

                continue;
            }

            $itemType = $this->itemTypeRepository->find($item->getType()->getId());
            if (empty($itemType)) {
                throw new ItemMustHaveAnExistingItemTypeException();
            }

            $item->setType($itemType);
            $item->setCreatedBy($user);
            $item->setUpdatedBy($user);
            $item->setValue($this->encryptHelper->encrypt($item->getValue()));

            $channel->addItem($item);
            $createdItems[] = $item;
        }

        if (!empty($createdItems)) {
            $this->channelRepository->persistAndFlush($channel);

            $this->esManager->getDocumentManager()->insertMultipleItems(
                $this->itemDocumentTranslator->fromEntitiesToDocuments($createdItems)
            );
        }

        if (!empty($notCreatedItems)) {
            throw new ItemsAlreadyExistsInChannelException($channelId, $notCreatedItems);
        }

        return $channel;
    }

    /**
     * @param string $userId
     * @param string $channelId
     *
     * @return bool
     *
     * @throws ChannelEntityNotFoundException
     * @throws UserNotFoundException
     */
    public function delete(string $userId, string $channelId): bool
    {
        $user = $this->userRepository->find($userId);
        if (null == $user) {
            throw new UserNotFoundException();
        }

        $channel = $this->channelRepository->find($channelId);
        if (null == $channel) {
            throw new ChannelEntityNotFoundException();
        }

        $this->channelRepository->delete($channel);
        $this->channelRepository->flush();

        return true;
    }
}
