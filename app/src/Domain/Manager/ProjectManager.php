<?php

namespace App\Domain\Manager;

use App\Domain\Entity\Channel;
use App\Domain\Entity\ItemType;
use App\Domain\Entity\Project;
use App\Domain\Entity\User;
use App\Domain\Exception\ProjectNotFoundException;
use App\Domain\Exception\UserIsNotActiveExcption;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Manager\Interfaces\ProjectManagerInterface;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;

/**
 * Class ProjectManager.
 */
class ProjectManager implements ProjectManagerInterface
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;
    /**
     * @var ItemTypeRepositoryInterface
     */
    private $itemTypeRepository;

    /**
     * ProjectManager constructor.
     *
     * @param ProjectRepositoryInterface  $projectRepository
     * @param UserRepositoryInterface     $userRepository
     * @param ChannelRepositoryInterface  $channelRepository
     * @param ItemTypeRepositoryInterface $itemTypeRepository
     */
    public function __construct(
        ProjectRepositoryInterface $projectRepository,
        UserRepositoryInterface $userRepository,
        ChannelRepositoryInterface $channelRepository,
        ItemTypeRepositoryInterface $itemTypeRepository
    ) {
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->channelRepository = $channelRepository;
        $this->itemTypeRepository = $itemTypeRepository;
    }

    /**
     * @param string $userId
     *
     * @return Project[]
     *
     * @throws UserIsNotActiveExcption
     * @throws UserNotFoundException
     */
    public function findByUser(string $userId): array
    {
        $user = $this->userRepository->find($userId);
        if (null == $user) {
            throw new UserNotFoundException();
        }
        if (!$user->isActive()) {
            throw new UserIsNotActiveExcption();
        }

        return $this->projectRepository->findByUserId($user->getId()->toString());
    }

    /**
     * @param User            $user
     * @param string          $projectId
     * @param array|Channel[] $channels
     *
     * @return array
     *
     * @throws ProjectNotFoundException
     */
    public function addChannels(User $user, string $projectId, array $channels): array
    {
        $project = $this->projectRepository->find($projectId);
        if (null === $project) {
            throw new ProjectNotFoundException($projectId);
        }

        $created = [];
        $notCreated = [];
        foreach ($channels as $channel) {
            $channelFromDb = $this->channelRepository->findByProjectIdAndChannelName($projectId, $channel->getName());
            if (null !== $channelFromDb) {
                $notCreated[] = [
                    'id' => $channel->getId()->toString(),
                    'name' => $channel->getName(),
                ];

                continue;
            }

            $channel->setCreatedBy($user);
            $channel->setUpdatedBy($user);

            $project->addChannel($channel);
            $created[] = [
                'id' => $channel->getId()->toString(),
                'name' => $channel->getName(),
            ];
        }

        if (!empty($created)) {
            $this->projectRepository->persistAndFlush($project);
        }

        return [
            'created' => $created,
            'already_exists' => $notCreated,
        ];
    }

    /**
     * @param string           $projectId
     * @param array|ItemType[] $itemTypes
     *
     * @return array
     *
     * @throws ProjectNotFoundException
     */
    public function addItemTypes(string $projectId, array $itemTypes): array
    {
        $project = $this->projectRepository->find($projectId);
        if (empty($project)) {
            throw new ProjectNotFoundException($projectId);
        }

        $created = [];
        $notCreated = [];

        foreach ($itemTypes as $itemType) {
            $itemTypeFromDb = $this->itemTypeRepository->findByName($itemType->getName());
            if (null !== $itemTypeFromDb) {
                $notCreated[] = [
                    'id' => $itemType->getId(),
                    'name' => $itemType->getName(),
                ];

                continue;
            }

            $project->addItemsType($itemType);
            $created[] = [
                'id' => $itemType->getId(),
                'name' => $itemType->getName(),
            ];
        }

        $this->projectRepository->persistAndFlush($project);

        return [
            'created' => $created,
            'already_exists' => $notCreated,
        ];
    }

    /**
     * @param string $userId
     * @param array  $projectIds
     *
     * @return User
     *
     * @throws ProjectNotFoundException
     * @throws UserNotFoundException
     */
    public function assignProjectToUser(string $userId, array $projectIds): User
    {
        $user = $this->userRepository->find($userId);
        if (null == $user) {
            throw new UserNotFoundException();
        }
        foreach ($projectIds as $projectId) {
            $project = $this->projectRepository->find($projectId);
            if (null == $project) {
                throw new ProjectNotFoundException($projectId);
            }
            $user->addProject($project);
        }

        $this->userRepository->persistAndFlush($user);

        return $user;
    }

    /**
     * @param string $userId
     * @param array  $projectIds
     *
     * @return User
     *
     * @throws UserNotFoundException
     */
    public function removeProjectsFromUser(string $userId, array $projectIds): User
    {
        $user = $this->userRepository->find($userId);
        if (null == $user) {
            throw new UserNotFoundException();
        }

        foreach ($projectIds as $projectId) {
            $project = $this->projectRepository->find($projectId);
            if (null !== $project) {
                $user->removeProject($project);
            }
        }

        $this->userRepository->persistAndFlush($user);

        return $user;
    }

    /**
     * @param string $userId
     * @param string $projectId
     *
     * @return bool
     *
     * @throws ProjectNotFoundException
     * @throws UserNotFoundException
     */
    public function delete(string $userId, string $projectId): bool
    {
        $user = $this->userRepository->find($userId);
        if (null == $user) {
            throw new UserNotFoundException();
        }

        $project = $this->projectRepository->find($projectId);
        if (null == $project) {
            throw new ProjectNotFoundException();
        }

        $this->projectRepository->delete($project);
        $this->projectRepository->flush();

        return true;
    }
}
