<?php

namespace App\Domain\Manager;

use App\Domain\Entity\ItemType;
use App\Domain\Manager\Interfaces\ItemTypeManagerInterface;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;

/**
 * Class ItemTypeManager.
 */
class ItemTypeManager implements ItemTypeManagerInterface
{
    /**
     * @var ItemTypeRepositoryInterface
     */
    private $itemTypeRepository;

    /**
     * ItemTypeManager constructor.
     *
     * @param ItemTypeRepositoryInterface $itemTypeRepository
     */
    public function __construct(
        ItemTypeRepositoryInterface $itemTypeRepository
    ) {
        $this->itemTypeRepository = $itemTypeRepository;
    }

    /**
     * @param string $projectId
     *
     * @return array|ItemType[]
     */
    public function findAll(string $projectId): array
    {
        return $this->itemTypeRepository->findByProjectId($projectId);
    }
}
