<?php

namespace App\Domain\Manager;

use App\Domain\Entity\Project;
use App\Domain\Entity\User;
use App\Domain\Exception\RoleNotFoundException;
use App\Domain\Exception\UserAlreadyExistsException;
use App\Domain\Manager\Interfaces\UserManagerInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\RoleRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Class UserManager.
 */
class UserManager implements UserManagerInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepository;
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;

    /**
     * UserManager constructor.
     *
     * @param UserRepositoryInterface    $userRepository
     * @param PasswordEncoderInterface   $passwordEncoder
     * @param RoleRepositoryInterface    $roleRepository
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        PasswordEncoderInterface $passwordEncoder,
        RoleRepositoryInterface $roleRepository,
        ProjectRepositoryInterface $projectRepository
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->roleRepository = $roleRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @return User[]
     */
    public function listUsers(): array
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param User $user
     *
     * @return User
     *
     * @throws RoleNotFoundException
     * @throws UserAlreadyExistsException
     */
    public function create(User $user): User
    {
        $userInDb = $this->userRepository->findOneBy(['email' => $user->getEmail()]);
        if (null !== $userInDb) {
            throw new UserAlreadyExistsException();
        }

        $role = $this->roleRepository->findOneBy(['name' => $user->getRole()->getName()]);
        if (null == $role) {
            throw new RoleNotFoundException();
        }
        $user->setRole($role);
        $user->setPassword($this->passwordEncoder->encodePassword($user->getPassword(), null));

        $this->userRepository->persistAndFlush($user);

        return $user;
    }

    /**
     * @param User            $user
     * @param array|Project[] $projects
     *
     * @return array
     */
    public function addProjects(User $user, array $projects): array
    {
        $created = [];
        $notCreated = [];

        foreach ($projects as $project) {
            $projectFromDb = $this->projectRepository->findOneBy(['name' => $project->getName()]);
            if (null != $projectFromDb) {
                $notCreated[] = [
                    'id' => $project->getId()->toString(),
                    'name' => $project->getName(),
                ];

                continue;
            }

            $user->addProject($project);
            $created[] = [
                'id' => $project->getId()->toString(),
                'name' => $project->getName(),
            ];
        }

        if (!empty($created)) {
            $this->userRepository->persistAndFlush($user);
        }

        return [
            'created' => $created,
            'already_exists' => $notCreated,
        ];
    }

    /**
     * @param string $email
     *
     * @return User|null
     */
    public function findByEmail(string $email): ?User
    {
        return $this->userRepository->findOneBy(['email' => $email]);
    }

    /**
     * @param string $id
     *
     * @return User|null
     */
    public function findById(string $id): ?User
    {
        return $this->userRepository->find($id);
    }
}
