<?php

namespace App\Domain\Manager;

use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\User;
use App\Domain\Exception\ErrorDeletingItemsException;
use App\Domain\Exception\ItemNotFoundException;
use App\Domain\Exception\UserNotFoundException;
use App\Domain\Manager\Interfaces\ItemManagerInterface;
use App\Domain\Translator\ItemDocumentTranslator;
use App\Infrastructure\ElasticSearch\ESFactory;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ItemRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use App\Infrastructure\Repository\Interfaces\UserRepositoryInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;

/**
 * Class itemManager.
 */
class ItemManager implements ItemManagerInterface
{
    /**
     * @var ItemRepositoryInterface
     */
    private $itemRepository;
    /**
     * @var ESFactory
     */
    private $esFactory;
    /**
     * @var ItemDocumentTranslator
     */
    private $itemDocumentTranslator;
    /**
     * @var ProjectRepositoryInterface
     */
    private $projectRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;

    /**
     * itemManager constructor.
     *
     * @param ItemRepositoryInterface    $itemRepository
     * @param ESFactory                  $esFactory
     * @param ItemDocumentTranslator     $itemDocumentTranslator
     * @param ProjectRepositoryInterface $projectRepository
     * @param UserRepositoryInterface    $userRepository
     * @param ChannelRepositoryInterface $channelRepository
     */
    public function __construct(
        ItemRepositoryInterface $itemRepository,
        ESFactory $esFactory,
        ItemDocumentTranslator $itemDocumentTranslator,
        ProjectRepositoryInterface $projectRepository,
        UserRepositoryInterface $userRepository,
        ChannelRepositoryInterface $channelRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->esFactory = $esFactory;
        $this->itemDocumentTranslator = $itemDocumentTranslator;
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->channelRepository = $channelRepository;
    }

    /**
     * @param User             $user
     * @param SearchItemFilter $filter
     *
     * @return Item|null
     *
     * @throws UnregisteredMappingException
     * @throws \Exception
     */
    public function findByFilter(User $user, SearchItemFilter $filter): ?Item
    {
        $esResult = $this->esFactory
            ->getDocumentManager()
            ->findByFilter(
                $this->itemDocumentTranslator->fromSearchItemFilterToSearchItemDocumentFilter($filter)
            );

        if (null !== $esResult) {
            return $this->itemDocumentTranslator->fromElasticSearchResultToItem($esResult);
        }

        return $this->itemRepository->findByFilter($filter);
    }

    /**
     * @param string $userId
     * @param array  $itemValues
     *
     * @return bool
     *
     * @throws ItemNotFoundException
     * @throws UserNotFoundException
     * @throws ErrorDeletingItemsException
     */
    public function delete(string $userId, array $itemValues): bool
    {
        $itemsToDelete = [];

        $user = $this->userRepository->find($userId);
        if (null == $user) {
            throw new UserNotFoundException();
        }
        foreach ($itemValues as $itemValue) {
            /** @var Item $item */
            $item = $this->itemRepository->findOneBy(['value' => $itemValue]);
            if (null == $item) {
                throw new ItemNotFoundException($itemValue);
            }

            $this->itemRepository->delete($item);
            $itemsToDelete[] = $item->getId()->toString();
        }

        try {
            $this->itemRepository->flush();
            $this->esFactory->getDocumentManager()->delete($itemsToDelete);
        } catch (\Exception $ex) {
            throw new ErrorDeletingItemsException($itemsToDelete);
        }

        return true;
    }

    /**
     * @param Item   $item
     * @param string $channelId
     *
     * @return Item
     *
     * @throws ItemNotFoundException
     * @throws UnregisteredMappingException
     */
    public function update(Item $item, string $channelId): Item
    {
        $itemFromDb = $this->itemRepository->findByChannelIdAndValue($channelId, $item->getValue());
        if (null == $itemFromDb) {
            throw new ItemNotFoundException($item->getValue());
        }

        $itemFromDb->setDescription($item->getDescription());
        $itemFromDb->setAccepted($item->isAccepted());

        $this->itemRepository->persistAndFlush($itemFromDb);

        $this->esFactory->getDocumentManager()->update(
            $this->itemDocumentTranslator->fromEntityToDocument($itemFromDb)
        );

        return $itemFromDb;
    }
}
