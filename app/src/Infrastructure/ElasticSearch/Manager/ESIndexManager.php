<?php

namespace App\Infrastructure\ElasticSearch\Manager;

use App\Infrastructure\ElasticSearch\Exception\CannotCreateIndexBecauseAlreadyExistsException;
use App\Infrastructure\ElasticSearch\Exception\MappingFileNotFoundException;
use Elasticsearch\Client;

/**
 * Class ESIndexManager.
 */
class ESIndexManager
{
    /**
     * @var string
     */
    private $indexName;

    /**
     * @var string
     */
    private $mappingFilePath;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     * @param string $indexName
     * @param string $mappingFilePath
     */
    public function setConfiguration(
        Client $client,
        string $indexName,
        string $mappingFilePath
    ): void {
        $this->client = $client;
        $this->indexName = $indexName;
        $this->mappingFilePath = $mappingFilePath;
    }

    /**
     * @return bool
     */
    public function exists(): bool
    {
        return $this->client->indices()->exists([
            'index' => $this->indexName,
        ]);
    }

    /**
     * @throws CannotCreateIndexBecauseAlreadyExistsException
     * @throws MappingFileNotFoundException
     */
    public function createIndexAndMapping(): void
    {
        if ($this->exists()) {
            throw new CannotCreateIndexBecauseAlreadyExistsException();
        }
        $this->client->indices()->create([
            'index' => $this->indexName,
        ]);

        if (!file_exists($this->mappingFilePath)) {
            throw new MappingFileNotFoundException();
        }
        $this->client->indices()->putMapping(
            json_decode(file_get_contents($this->mappingFilePath), true)
        );
    }

    public function delete(): void
    {
        $this->client->indices()->delete([
            'index' => $this->indexName,
        ]);
    }
}
