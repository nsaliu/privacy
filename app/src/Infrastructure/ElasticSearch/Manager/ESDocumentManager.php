<?php

namespace App\Infrastructure\ElasticSearch\Manager;

use App\Infrastructure\ElasticSearch\Entity\Filter\SearchItemDocumentFilter;
use App\Infrastructure\ElasticSearch\Entity\ItemDocument;
use Elasticsearch\Client;

/**
 * Class ESDocumentManager.
 */
class ESDocumentManager
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $indexName;

    /**
     * @param Client $client
     * @param string $indexName
     */
    public function setConfiguration(
        Client $client,
        string $indexName
    ) {
        $this->client = $client;
        $this->indexName = $indexName;
    }

    /**
     * @param array|ItemDocument[] $items
     */
    public function insertMultipleItems(array $items): void
    {
        $params = ['body' => []];

        foreach ($items as $item) {
            $params['body'][] = [
                'index' => [
                    '_index' => $this->indexName,
                    '_id' => $item->getId(),
                ],
            ];

            $params['body'][] = [
                'project_id' => $item->getProjectId(),
                'channel_id' => $item->getChannelId(),
                'type' => $item->getType(),
                'value' => $item->getValue(),
                'is_accepted' => $item->isAccepted(),
                'description' => $item->getDescription(),
                'created_at' => $item->getCreatedAt()->format('Y-m-d'),
                'updated_at' => $item->getUpdatedAt()->format('Y-m-d'),
            ];
        }

        $this->client->bulk($params);
    }

    /**
     * @param ItemDocument $item
     */
    public function insertSingleItem(ItemDocument $item): void
    {
        $this->client->index([
            'index' => $this->indexName,
            'id' => $item->getId(),
            'body' => [
                'project_id' => $item->getProjectId(),
                'channel_id' => $item->getChannelId(),
                'type' => $item->getType(),
                'value' => $item->getValue(),
                'is_accepted' => $item->isAccepted(),
                'description' => $item->getDescription(),
                'created_at' => $item->getCreatedAt()->format('Y-m-d'),
                'updated_at' => $item->getUpdatedAt()->format('Y-m-d'),
            ],
        ]);
    }

    /**
     * @param SearchItemDocumentFilter $filter
     *
     * @return ItemDocument|null
     *
     * @throws \Exception
     */
    public function findByFilter(SearchItemDocumentFilter $filter): ?ItemDocument
    {
        $result = $this->client->search([
            'index' => $this->indexName,
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            ['match' => ['channel_id' => $filter->getChannelId()]],
                            ['match' => ['value' => $filter->getItemValue()]],
                        ],
                    ],
                ],
            ],
        ]);

        if (empty($result) || (empty($result['hits']['total']['value']) || 0 === $result['hits']['total']['value'])) {
            return null;
        }

        return new ItemDocument(
            $result['hits']['hits'][0]['_source']['project_id'],
            $result['hits']['hits'][0]['_source']['channel_id'],
            $result['hits']['hits'][0]['_id'],
            $result['hits']['hits'][0]['_source']['type'],
            $result['hits']['hits'][0]['_source']['value'],
            $result['hits']['hits'][0]['_source']['is_accepted'],
            $result['hits']['hits'][0]['_source']['description'],
            new \DateTime($result['hits']['hits'][0]['_source']['created_at']),
            new \DateTime($result['hits']['hits'][0]['_source']['created_at'])
        );
    }

    /**
     * @param array $documentIds
     */
    public function delete(array $documentIds): void
    {
        foreach ($documentIds as $documentId) {
            $this->client->delete([
                'index' => $this->indexName,
                'id' => $documentId,
            ]);
        }
    }

    /**
     * @param ItemDocument $item
     */
    public function update(ItemDocument $item): void
    {
        $this->client->update([
            'index' => $this->indexName,
            'id' => $item->getId(),
            'body' => [
                'doc' => [
                    'description' => $item->getDescription(),
                    'is_accepted' => $item->isAccepted(),
                    'updated_at' => $item->getUpdatedAt()->format('Y-m-d'),
                ],
            ],
        ]);
    }
}
