<?php

namespace App\Infrastructure\ElasticSearch\Exception;

/**
 * Class CannotDeleteIndexBeacauseNotExistsException.
 */
class CannotDeleteIndexBecauseNotExistsException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Cannot delete specified index because does not exists';
}
