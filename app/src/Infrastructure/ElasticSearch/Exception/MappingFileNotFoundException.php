<?php

namespace App\Infrastructure\ElasticSearch\Exception;

/**
 * Class MappingFileNotFoundException.
 */
class MappingFileNotFoundException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Mapping file not found';
}
