<?php

namespace App\Infrastructure\ElasticSearch\Exception;

/**
 * Class CannotCreateIndexBecauseAlreadyExistsException.
 */
class CannotCreateIndexBecauseAlreadyExistsException extends \Exception
{
    /**
     * @var string
     */
    protected $message = 'Cannot create index because specified index already exists';
}
