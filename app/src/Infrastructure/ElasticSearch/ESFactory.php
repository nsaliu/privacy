<?php

namespace App\Infrastructure\ElasticSearch;

use App\Infrastructure\ElasticSearch\Manager\ESDocumentManager;
use App\Infrastructure\ElasticSearch\Manager\ESIndexManager;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class ESFactory.
 */
class ESFactory
{
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var ESIndexManager
     */
    private $indexManager;

    /**
     * @var ESDocumentManager
     */
    private $documentManager;

    /**
     * ESFactory constructor.
     *
     * @param ParameterBagInterface $parameterBag
     * @param ClientBuilder         $clientBuilder
     * @param ESIndexManager        $indexManager
     * @param ESDocumentManager     $documentManager
     */
    public function __construct(
        ParameterBagInterface $parameterBag,
        ClientBuilder $clientBuilder,
        ESIndexManager $indexManager,
        ESDocumentManager $documentManager
    ) {
        $this->parameterBag = $parameterBag;
        $this->indexManager = $indexManager;

        $clientBuilder->setHosts([$this->parameterBag->get('app.elasticsearch.endpoint')]);
        $this->client = $clientBuilder->build();
        $this->documentManager = $documentManager;
    }

    /**
     * @return ESIndexManager
     */
    public function getIndexManager(): ESIndexManager
    {
        $this->indexManager->setConfiguration(
            $this->client,
            $this->parameterBag->get('app.elasticsearch.index.name'),
            $this->parameterBag->get('app.elasticsearch.mapping.source.file')
        );

        return $this->indexManager;
    }

    /**
     * @return ESDocumentManager
     */
    public function getDocumentManager(): ESDocumentManager
    {
        $this->documentManager->setConfiguration(
            $this->client,
            $this->parameterBag->get('app.elasticsearch.index.name')
        );

        return $this->documentManager;
    }
}
