<?php

namespace App\Infrastructure\ElasticSearch\Entity\Filter;

/**
 * Class SearchItemDocumentFilter.
 */
final class SearchItemDocumentFilter
{
    /**
     * @var string
     */
    private $channelId;
    /**
     * @var string
     */
    private $itemValue;

    /**
     * SearchItemDocumentFilter constructor.
     *
     * @param string $channelId
     * @param string $itemValue
     */
    public function __construct(
        string $channelId,
        string $itemValue
    ) {
        $this->channelId = $channelId;
        $this->itemValue = $itemValue;
    }

    /**
     * @return string
     */
    public function getChannelId(): string
    {
        return $this->channelId;
    }

    /**
     * @return string
     */
    public function getItemValue(): string
    {
        return $this->itemValue;
    }
}
