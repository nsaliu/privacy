<?php

namespace App\Infrastructure\ElasticSearch\Entity;

/**
 * Class ItemDocument.
 */
final class ItemDocument
{
    /**
     * @var string
     */
    private $projectId;
    /**
     * @var string
     */
    private $channelId;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $value;
    /**
     * @var bool
     */
    private $accepted;
    /**
     * @var string
     */
    private $description;
    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;
    /**
     * @var \DateTimeImmutable
     */
    private $updatedAt;

    /**
     * ItemDocument constructor.
     *
     * @param string    $projectId
     * @param string    $channelId
     * @param string    $id
     * @param string    $type
     * @param string    $value
     * @param bool      $accepted
     * @param string    $description
     * @param \DateTime $createdAt
     * @param \DateTime $updatedAt
     */
    public function __construct(
        string $projectId,
        string $channelId,
        string $id,
        string $type,
        string $value,
        bool $accepted,
        string $description,
        \DateTime $createdAt,
        \DateTime $updatedAt
    ) {
        $this->projectId = $projectId;
        $this->channelId = $channelId;
        $this->id = $id;
        $this->type = $type;
        $this->value = $value;
        $this->accepted = $accepted;
        $this->description = $description;
        $this->createdAt = \DateTimeImmutable::createFromMutable($createdAt);
        $this->updatedAt = \DateTimeImmutable::createFromMutable($updatedAt);
    }

    /**
     * @return string
     */
    public function getProjectId(): string
    {
        return $this->projectId;
    }

    /**
     * @return string
     */
    public function getChannelId(): string
    {
        return $this->channelId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isAccepted(): bool
    {
        return $this->accepted;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
