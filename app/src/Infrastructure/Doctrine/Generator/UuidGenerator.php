<?php

namespace App\Infrastructure\Doctrine\Generator;

use App\Domain\Entity\PersistibleDomainEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class CustomUuidGenerator.
 */
class UuidGenerator extends AbstractIdGenerator
{
    /**
     * @param EntityManager $em
     * @param object|null   $entity
     *
     * @return mixed|UuidInterface
     *
     * @throws \Exception
     */
    public function generate(EntityManager $em, $entity)
    {
        if (!empty($entity) &&
            is_a($entity, PersistibleDomainEntity::class) &&
            (null !== $entity->getId() && is_a($entity->getId(), UuidInterface::class))
        ) {
            return $entity->getId();
        }

        return Uuid::uuid4();
    }
}
