<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Infrastructure\Repository\Interfaces\ItemRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends BaseRepository implements ItemRepositoryInterface
{
    /**
     * ItemRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Item::class);
    }

    /**
     * @param string $channelId
     *
     * @return Item[]
     */
    public function findByChanelId(string $channelId): array
    {
        return $this->findBy(
            [
                'channel' => $channelId,
            ]
        );
    }

    /**
     * @param SearchItemFilter $filter
     *
     * @return Item
     */
    public function findByFilter(SearchItemFilter $filter): ?Item
    {
        return $this->findOneBy([
            'channel' => $filter->getChannelId(),
            'value' => $filter->getItemValue(),
        ]);
    }

    /**
     * @param string $channelId
     * @param string $itemValue
     *
     * @return Item|null
     *
     * @throws NonUniqueResultException
     */
    public function findByChannelIdAndValue(string $channelId, string $itemValue): ?Item
    {
        return $this->createQueryBuilder('i')
            ->join('i.channel', 'c')
            ->where('i.value = :item_value')
            ->andWhere('c.id = :channel_id')
            ->setParameter(':item_value', $itemValue)
            ->setParameter(':channel_id', $channelId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param mixed $entity
     *
     * @return mixed|void
     *
     * @throws ORMException
     */
    public function delete($entity)
    {
        $this->remove($entity);
    }
}
