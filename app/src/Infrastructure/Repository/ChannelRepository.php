<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Channel;
use App\Infrastructure\Repository\Interfaces\ChannelRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Channel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Channel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Channel[]    findAll()
 * @method Channel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelRepository extends BaseRepository implements ChannelRepositoryInterface
{
    /**
     * ChannelRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Channel::class);
    }

    /**
     * @param string $projectId
     *
     * @return Channel[]
     */
    public function findByProjectId(string $projectId): array
    {
        return $this->findBy(['project' => $projectId]);
    }

    /**
     * @param string $projectId
     * @param string $channelName
     *
     * @return Channel|null
     *
     * @throws NonUniqueResultException
     */
    public function findByProjectIdAndChannelName(string $projectId, string $channelName): ?Channel
    {
        return $this->createQueryBuilder('c')
            ->join('c.project', 'p')
            ->where('p.id = :project_id')
            ->andWhere('c.name = :channel_name')
            ->setParameter(':project_id', $projectId)
            ->setParameter(':channel_name', $channelName)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param mixed $entity
     *
     * @throws ORMException
     */
    public function delete($entity)
    {
        $this->remove($entity);
    }
}
