<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Entity\Channel;
use App\Domain\Entity\PersistibleDomainEntity;

/**
 * Interfaces ChannelRepositoryInterface.
 */
interface ChannelRepositoryInterface
{
    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persist(PersistibleDomainEntity $entity);

    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persistAndFlush(PersistibleDomainEntity $entity);

    /**
     * @param string $id
     * @param null   $lockMode
     * @param null   $lockVersion
     *
     * @return Channel
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return Channel
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return Channel[]
     */
    public function findAll();

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return Channel[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param string $projectId
     *
     * @return Channel[]
     */
    public function findByProjectId(string $projectId): array;

    /**
     * @param string $projectId
     * @param string $channelName
     *
     * @return Channel|null
     */
    public function findByProjectIdAndChannelName(string $projectId, string $channelName): ?Channel;

    /**
     * @param mixed $entity
     */
    public function delete($entity);

    /**
     * @return mixed
     */
    public function flush();
}
