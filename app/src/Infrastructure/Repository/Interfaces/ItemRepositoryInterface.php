<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Entity\Filter\SearchItemFilter;
use App\Domain\Entity\Item;
use App\Domain\Entity\PersistibleDomainEntity;

/**
 * Interfaces ItemRepositoryInterface.
 */
interface ItemRepositoryInterface
{
    /**
     * @param string $id
     * @param null   $lockMode
     * @param null   $lockVersion
     *
     * @return Item
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return Item
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return Item[]
     */
    public function findAll();

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return Item[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param mixed $entity
     *
     * @return mixed
     */
    public function delete($entity);

    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persistAndFlush(PersistibleDomainEntity $entity);

    /**
     * @return mixed
     */
    public function flush();

    /**
     * @param string $channelId
     *
     * @return Item[]
     */
    public function findByChanelId(string $channelId): array;

    /**
     * @param SearchItemFilter $filter
     *
     * @return Item
     */
    public function findByFilter(SearchItemFilter $filter): ?Item;

    /**
     * @param string $channelId
     * @param string $itemValue
     *
     * @return Item|null
     */
    public function findByChannelIdAndValue(string $channelId, string $itemValue): ?Item;
}
