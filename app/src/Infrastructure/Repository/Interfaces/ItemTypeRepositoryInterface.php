<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Entity\ItemType;

/**
 * Class ItemTypeRepositoryInterface.
 */
interface ItemTypeRepositoryInterface
{
    /**
     * @param string $id
     * @param null   $lockMode
     * @param null   $lockVersion
     *
     * @return ItemType
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return ItemType
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return ItemType[]
     */
    public function findAll();

    /**
     * @param string $projectId
     *
     * @return array
     */
    public function findByProjectId(string $projectId): array;

    /**
     * @param string $name
     *
     * @return ItemType|null
     */
    public function findByName(string $name): ?ItemType;
}
