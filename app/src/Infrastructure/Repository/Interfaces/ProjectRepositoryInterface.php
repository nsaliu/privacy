<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Entity\PersistibleDomainEntity;
use App\Domain\Entity\Project;

/**
 * Interfaces ProjectRepositoryInterface.
 */
interface ProjectRepositoryInterface
{
    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persist(PersistibleDomainEntity $entity);

    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persistAndFlush(PersistibleDomainEntity $entity);

    /**
     * @param string $id
     * @param null   $lockMode
     * @param null   $lockVersion
     *
     * @return Project|null
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return Project
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return Project[]
     */
    public function findAll();

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return Project[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param string $userId
     *
     * @return Project[]
     */
    public function findByUserId(string $userId): array;

    /**
     * @param string $userId
     * @param string $projectId
     *
     * @return Project|null
     */
    public function findByUserIdAndProjectId(string $userId, string $projectId): ?Project;

    /**
     * @param mixed $entity
     *
     * @return mixed
     */
    public function delete($entity);

    /**
     * @return mixed
     */
    public function flush();
}
