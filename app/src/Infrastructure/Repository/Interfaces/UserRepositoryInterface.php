<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Entity\PersistibleDomainEntity;
use App\Domain\Entity\User;

/**
 * Interfaces UserRepositoryInterface.
 */
interface UserRepositoryInterface
{
    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persist(PersistibleDomainEntity $entity);

    /**
     * @param PersistibleDomainEntity $entity
     *
     * @return mixed
     */
    public function persistAndFlush(PersistibleDomainEntity $entity);

    /**
     * @param string $id
     * @param null   $lockMode
     * @param null   $lockVersion
     *
     * @return User
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return User
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return User[]
     */
    public function findAll();

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return User[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
}
