<?php

namespace App\Infrastructure\Repository\Interfaces;

use App\Domain\Entity\Role;

/**
 * Interfaces RoleRepositoryInterface.
 */
interface RoleRepositoryInterface
{
    /**
     * @param string $id
     * @param null   $lockMode
     * @param null   $lockVersion
     *
     * @return Role
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return Role|null
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return Role[]
     */
    public function findAll();

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return Role[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param Role $role
     */
    public function create(Role $role): void;
}
