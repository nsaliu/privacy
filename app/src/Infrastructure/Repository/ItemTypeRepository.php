<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\ItemType;
use App\Infrastructure\Repository\Interfaces\ItemTypeRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ItemType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemType[]    findAll()
 * @method ItemType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemTypeRepository extends ServiceEntityRepository implements ItemTypeRepositoryInterface
{
    /**
     * ItemRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ItemType::class);
    }

    /**
     * @param string $projectId
     *
     * @return array
     */
    public function findByProjectId(string $projectId): array
    {
        return $this->findBy(['project' => $projectId], ['name' => 'ASC']);
    }

    /**
     * @param string $name
     *
     * @return ItemType|null
     */
    public function findByName(string $name): ?ItemType
    {
        return $this->findOneBy(['name' => $name]);
    }
}
