<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\Project;
use App\Infrastructure\Repository\Interfaces\ProjectRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends BaseRepository implements ProjectRepositoryInterface
{
    /**
     * ProjectRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Project::class);
    }

    /**
     * @param string $userId
     *
     * @return array
     */
    public function findByUserId(string $userId): array
    {
        return $this->createQueryBuilder('p')
            ->join('p.users', 'u')
            ->where('u.id = :user_id')
            ->andWhere('u.active = :user_active')
            ->setParameter(':user_id', $userId)
            ->setParameter(':user_active', true)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $userId
     * @param string $projectId
     *
     * @return Project|null
     *
     * @throws NonUniqueResultException
     */
    public function findByUserIdAndProjectId(string $userId, string $projectId): ?Project
    {
        return $this->createQueryBuilder('p')
            ->join('p.users', 'u')
            ->where('u.id = :user_id')
            ->andWhere('p.id = :project_id')
            ->andWhere('u.active = :user_active')
            ->setParameter(':user_id', $userId)
            ->setParameter(':project_id', $projectId)
            ->setParameter(':user_active', 1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param mixed $entity
     *
     * @return mixed|void
     *
     * @throws ORMException
     */
    public function delete($entity)
    {
        $this->remove($entity);
    }
}
