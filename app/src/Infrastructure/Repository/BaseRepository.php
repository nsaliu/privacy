<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\PersistibleDomainEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class BaseRepository.
 */
abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @param PersistibleDomainEntity $entity
     *
     * @throws ORMException
     */
    public function persist(PersistibleDomainEntity $entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @param PersistibleDomainEntity $entity
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function persistAndFlush(PersistibleDomainEntity $entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    /**
     * @param mixed $entity
     *
     * @throws ORMException
     */
    public function remove($entity)
    {
        $this->getEntityManager()->remove($entity);
    }
}
