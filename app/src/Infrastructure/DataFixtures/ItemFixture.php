<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * Class ItemFixture.
 */
class ItemFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->getItem_1());
        $manager->persist($this->getItem_2());
        $manager->flush();
    }

    /**
     * @return Item
     *
     * @throws \Exception
     */
    private function getItem_1()
    {
        $item = new Item();
        $item->setId(Uuid::fromString('657abc74-bb6e-408b-9c70-66e6deebe63c'));
        $item->setType($this->getReference(ItemTypeFixture::ITEM_TYPE_REFERENCE_EMAIL));
        $item->setAccepted(true);
        $item->setDescription('Item description');
        $item->setValue('Item value 1');
        $item->setChannel($this->getReference(ChannelFixture::CHANNEL_REFERENCE));
        $item->setCreatedBy($this->getReference(UserFixture::USER_REFERENCE));
        $item->setUpdatedBy($this->getReference(UserFixture::USER_REFERENCE));

        return $item;
    }

    /**
     * @return Item
     *
     * @throws \Exception
     */
    private function getItem_2()
    {
        $item = new Item();
        $item->setId(Uuid::fromString('8e7dfb27-5fb1-4a2e-b54d-9446295a560b'));
        $item->setType($this->getReference(ItemTypeFixture::ITEM_TYPE_REFERENCE_PHONE));
        $item->setAccepted(true);
        $item->setDescription('Item description');
        $item->setValue('Item value 2');
        $item->setChannel($this->getReference(ChannelFixture::CHANNEL_REFERENCE));
        $item->setCreatedBy($this->getReference(UserFixture::USER_REFERENCE));
        $item->setUpdatedBy($this->getReference(UserFixture::USER_REFERENCE));

        return $item;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixture::class,
            ChannelFixture::class,
        ];
    }
}
