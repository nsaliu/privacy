<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\Role;
use App\Domain\Enum\RoleEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class RoleReaderFixture.
 */
class RoleFixture extends Fixture
{
    public const ROLE_READER = 'role_reader_reference';
    public const ROLE_WRITER = 'role_witer_reference';
    public const ROLE_READER_AND_WRITER = 'role_reader_and_writer_reference';

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $readerRole = new Role();
        $readerRole->setName(RoleEnum::READER);

        $writerRole = new Role();
        $writerRole->setName(RoleEnum::WRITER);

        $readerAndWriterRole = new Role();
        $readerAndWriterRole->setName(RoleEnum::ADMIN);

        $manager->persist($readerRole);
        $manager->persist($writerRole);
        $manager->persist($readerAndWriterRole);
        $manager->flush();

        $this->addReference(self::ROLE_READER, $readerRole);
        $this->addReference(self::ROLE_WRITER, $writerRole);
        $this->addReference(self::ROLE_READER_AND_WRITER, $readerAndWriterRole);
    }
}
