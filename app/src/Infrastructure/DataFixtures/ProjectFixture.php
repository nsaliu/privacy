<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * Class ProjectFixture.
 */
class ProjectFixture extends Fixture implements DependentFixtureInterface
{
    public const UUID_1 = '43501cf5-6ce8-464a-92e6-0ddde4757985';
    public const UUID_2 = '60dd28ae-73c7-4e02-837e-ae6723d7ffb5';

    public const PROJECT_REFERENCE = 'project_reference';
    public const PROJECT_1_REFERENCE = 'project_1_reference';
    public const PROJECT_2_REFERENCE = 'project_2_reference';

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $project_1 = $this->getProject_1();
        $project_2 = $this->getProject_2();

        $manager->persist($project_1);
        $manager->persist($project_2);
        $manager->flush();

        $this->addReference(self::PROJECT_REFERENCE, $project_1);
        $this->addReference(self::PROJECT_1_REFERENCE, $project_1);
        $this->addReference(self::PROJECT_2_REFERENCE, $project_2);
    }

    /**
     * @return Project
     *
     * @throws \Exception
     */
    private function getProject_1(): Project
    {
        $project = new Project();
        $project->setId(Uuid::fromString(self::UUID_1));
        $project->setDescription('Project 1 description');
        $project->setName('Project 1');
        $project->setCreatedBy($this->getReference(UserFixture::USER_REFERENCE));
        $project->setUpdatedBy($this->getReference(UserFixture::USER_REFERENCE));

        return $project;
    }

    /**
     * @return Project
     *
     * @throws \Exception
     */
    private function getProject_2(): Project
    {
        $project = new Project();
        $project->setId(Uuid::fromString(self::UUID_2));
        $project->setDescription('Project 2 description');
        $project->setName('Project 2');
        $project->setCreatedBy($this->getReference(UserFixture::USER_REFERENCE));
        $project->setUpdatedBy($this->getReference(UserFixture::USER_REFERENCE));

        return $project;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixture::class,
        ];
    }
}
