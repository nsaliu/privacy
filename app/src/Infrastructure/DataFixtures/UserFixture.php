<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Class UserFixture.
 */
class UserFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @var PasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserFixture constructor.
     *
     * @param PasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        PasswordEncoderInterface $passwordEncoder
    ) {
        $this->passwordEncoder = $passwordEncoder;
    }

    public const USER_REFERENCE = 'user_reference';

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setId(Uuid::fromString('464f9a71-88de-4424-805b-caf348bf9e23'));
        $user->setEmail('user.api@priva.io');
        $user->setPassword($this->passwordEncoder->encodePassword('secret_password', $user->getSalt()));
        $user->setRole($this->getReference(RoleFixture::ROLE_READER_AND_WRITER));

        $manager->persist($user);
        $manager->flush();

        $this->addReference(self::USER_REFERENCE, $user);
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            RoleFixture::class,
        ];
    }
}
