<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\ItemType;
use App\Domain\Enum\ItemTypeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * Class ItemTypeFixture.
 */
class ItemTypeFixture extends Fixture implements DependentFixtureInterface
{
    public const ITEM_TYPE_REFERENCE_EMAIL = 'item_type_reference_email';
    public const ITEM_TYPE_REFERENCE_PHONE = 'item_type_reference_phone';

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $itemEmail = $this->createItemTypeEmail();
        $itemPhone = $this->createItemTypePhone();

        $manager->persist($itemEmail);
        $manager->persist($itemPhone);
        $manager->flush();
    }

    /**
     * @return ItemType
     *
     * @throws \Exception
     */
    private function createItemTypeEmail(): ItemType
    {
        $itemType = new ItemType();
        $itemType->setId(Uuid::fromString('648ffb9f-2bd8-4a6a-949d-fef8c44f439f'));
        $itemType->setName(ItemTypeEnum::EMAIL);
        $itemType->setProject($this->getReference(ProjectFixture::PROJECT_REFERENCE));

        $this->addReference(self::ITEM_TYPE_REFERENCE_EMAIL, $itemType);

        return $itemType;
    }

    /**
     * @return ItemType
     *
     * @throws \Exception
     */
    private function createItemTypePhone(): ItemType
    {
        $itemType = new ItemType();
        $itemType->setId(Uuid::fromString('ac6e5fde-621e-4a4a-b44b-27ef33afd531'));
        $itemType->setName(ItemTypeEnum::PHONE);
        $itemType->setProject($this->getReference(ProjectFixture::PROJECT_REFERENCE));

        $this->addReference(self::ITEM_TYPE_REFERENCE_PHONE, $itemType);

        return $itemType;
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            ProjectFixture::class,
        ];
    }
}
