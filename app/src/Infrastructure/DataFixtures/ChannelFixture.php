<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\Channel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

/**
 * Class ChannelFixture.
 */
class ChannelFixture extends Fixture implements DependentFixtureInterface
{
    public const UUID = '401afb60-a2c0-4943-a470-5cce1ea9218b';

    public const CHANNEL_REFERENCE = 'channel_reference';

    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $channel = new Channel();
        $channel->setId(Uuid::fromString(self::UUID));
        $channel->setName('Channel 1');
        $channel->setDescription('Channel description');
        $channel->setType('Marketing');
        $channel->setProject($this->getReference(ProjectFixture::PROJECT_REFERENCE));
        $channel->setCreatedBy($this->getReference(UserFixture::USER_REFERENCE));
        $channel->setUpdatedBy($this->getReference(UserFixture::USER_REFERENCE));

        $manager->persist($channel);
        $manager->flush();

        $this->addReference(self::CHANNEL_REFERENCE, $channel);
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixture::class,
            ProjectFixture::class,
        ];
    }
}
