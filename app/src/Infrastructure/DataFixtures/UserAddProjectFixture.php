<?php

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\Project;
use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class UserAddProjectFixture.
 */
class UserAddProjectFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $this->getReference(UserFixture::USER_REFERENCE);

        /** @var Project $project_1 */
        $project_1 = $this->getReference(ProjectFixture::PROJECT_1_REFERENCE);

        /** @var Project $project_2 */
        $project_2 = $this->getReference(ProjectFixture::PROJECT_2_REFERENCE);

        $user->addProject($project_1);
        $user->addProject($project_2);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixture::class,
            ProjectFixture::class,
        ];
    }
}
