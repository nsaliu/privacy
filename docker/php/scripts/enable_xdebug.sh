#!/bin/sh

echo "Enabling Xdebug extension ..."
mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini.disabled /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
kill -USR2 1
echo "Xdebug extension enabled."