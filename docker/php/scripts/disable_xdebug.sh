#!/bin/sh

echo "Disabling Xdebug extension ..."
mv /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini.disabled
kill -USR2 1
echo "Xdebug extension disabled."