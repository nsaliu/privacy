# Privar.io
A centralized service for storing and querying users privacy preferences.

### First run (installation)
To install Privar.io you have to:
- checkout repository
- enter the root directory of the project
- type ```make install```

### Start the environment
After installation, you have to run ```make start``` in order to start the env.

### Useful commands
In ```Makefile``` you can find some useful commands to interact with the project.

### Stop the environment
To stop env you have to run ```make stop```.

### Create your own ssh keys  
By default test ssh keys are stored in ```app/config/jwt/``` directory.  
Please change it if your want to run Privar.io in prod.  
Please refer to the LexikJWTAuthenticationBundle: https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#generate-the-ssh-keys.  
Remeber to change also your ```JWT_PASSPHRASE``` in ```.env*``` file.

## Available APIs

### User management

#### Login with credentials
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/login   
Body:
```json
{
  "email": "user.api@priva.io",
  "password": "secret_password"
}
```
Response:  
```json
{
    "code": 200,
    "message": "welcome!",
    "data": [
        {
            "token": "<jwt token>"
        }
    ]
}
```

#### List all Users
Content-Type: application/json  
Method: GET  
Endpoint: http://{{base_url}}/api/users  
Response:
```json
{
    "code": 200,
    "data": [
        [
            {
                "id": "464f9a71-88de-4424-805b-caf348bf9e23",
                "email": "user.api@priva.io"
            }
        ]
    ]
}
```

#### Create a User
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/users  
Body:
```json
{
	"email": "2.user.api@priva.io",
	"password": "S3cr3tP455@rD",
	"role": "reader",
	"active": true
}
```  
Response:
```json
{
    "code": 201,
    "message": "created",
    "data": []
}
```

### Project management

#### Create a Project
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/projects  
Body:
```json
{
	"projects": [
		{
			"name": "prj 1",
			"description": "descr prj 1..."
		},
		{
			"name": "prj 2",
			"description": "descr prj 2..."
		}
	]
}
```  
Response:
```json
{
    "code": 201,
    "message": "created",
    "data": [
        {
            "created": [
                {
                    "id": "da8a97af-1d2f-4ebb-8b1f-56c739e043df",
                    "name": "prj 1"
                },
                {
                    "id": "77692845-b802-4044-b883-c61eca837b0d",
                    "name": "prj 2"
                }
            ],
            "already_exists": []
        }
    ]
}
```

#### List all Projects by User id
Content-Type: application/json  
Method: GET  
Endpoint: http://{{base_url}}/api/projects/<user id>  
Response:
```json
{
    "code": 200,
    "data": [
        [
            {
                "id": "43501cf5-6ce8-464a-92e6-0ddde4757985",
                "name": "Project 1",
                "description": "Project 1 description"
            },
            {
                "id": "60dd28ae-73c7-4e02-837e-ae6723d7ffb5",
                "name": "Project 2",
                "description": "Project 2 description"
            }
        ]
    ]
}
```

#### Assign a one or more Projects to a User
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/projects-assign-to-user
Body:
```json
{
	"user_id": "464f9a71-88de-4424-805b-caf348bf9e23",
	"project_ids": [
		"43501cf5-6ce8-464a-92e6-0ddde4757985",
		"60dd28ae-73c7-4e02-837e-ae6723d7ffb5"
	]
}
```  
Response:
```json
{
    "code": 200,
    "message": "associated",
    "data": []
}
```

#### Remove one or more Projects to a User
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/projects-remove-from-user
Body:
```json
{
	"user_id": "464f9a71-88de-4424-805b-caf348bf9e23",
	"project_ids": [
		"43501cf5-6ce8-464a-92e6-0ddde4757985"
	]
}
```  
Response:
```json
{
    "code": 200,
    "message": "removed",
    "data": []
}
```

#### Delete a Project
Content-Type: application/json  
Method: DELETE  
Endpoint: http://{{base_url}}/api/projects-delete
Body:
```json
{
	"project_id": "43501cf5-6ce8-464a-92e6-0ddde4757985"
}
```  
Response:
```json
{
    "code": 200,
    "message": "deleted",
    "data": []
}
```

### Channels management

#### Create a Channel
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/channels
Body:
```json
{
	"project_id": "43501cf5-6ce8-464a-92e6-0ddde4757985",
	"channels": [
		{
			"name": "test ch 4",
			"type": "test type 1",
			"description": "test desc 1",
			"accepted": true
		},
		{
			"name": "test ch 5",
			"type": "test type 2",
			"description": "test desc 2",
			"accepted": true
		},
		{
			"name": "test ch 6",
			"type": "test type 3",
			"description": "test desc 3",
			"accepted": true
		}
	]
}
```  
Response:
```json
{
    "code": 201,
    "message": "created",
    "data": [
        {
            "created": [
                {
                    "id": "b07f0d8f-275a-424e-9269-3bee0d732878",
                    "name": "test ch 4"
                },
                {
                    "id": "2d7c5aaa-6524-4a52-93af-6d6fbec7de2c",
                    "name": "test ch 5"
                },
                {
                    "id": "4215a879-118b-437e-a7fb-de5c65c070a7",
                    "name": "test ch 6"
                }
            ],
            "already_exists": []
        }
    ]
}
```

#### List Channels by Project id
Content-Type: application/json  
Method: GET  
Endpoint: http://{{base_url}}/api/channels/<project id>
Response:
```json
{
    "code": 200,
    "data": [
        [
            {
                "id": "2d7c5aaa-6524-4a52-93af-6d6fbec7de2c",
                "name": "test ch 5",
                "type": "test type 2",
                "description": "test desc 2",
                "accepted": null
            },
            {
                "id": "401afb60-a2c0-4943-a470-5cce1ea9218b",
                "name": "Channel 1",
                "type": "Marketing",
                "description": "Channel description",
                "accepted": null
            }
        ]
    ]
}
```

#### Get Channel details by id
Content-Type: application/json  
Method: GET  
Endpoint: http://{{base_url}}/api/channel/<channel id>
Response:
```json
{
    "code": 200,
    "data": [
        {
            "id": "401afb60-a2c0-4943-a470-5cce1ea9218b",
            "name": "Channel 1",
            "type": "Marketing",
            "description": "Channel description",
            "accepted": null,
            "items": {}
        }
    ]
}
```

#### Delete a Channel
Content-Type: application/json  
Method: DELETE  
Endpoint: http://{{base_url}}/api/channel-delete
Response:
```json
{
    "code": 200,
    "message": "deleted",
    "data": []
}
```

### Items management

#### Create one or more Items
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/items  
Body:
```json
{
	"channel_id": "401afb60-a2c0-4943-a470-5cce1ea9218b",
	"items": [
		{
			"item_type_id": "648ffb9f-2bd8-4a6a-949d-fef8c44f439f",
			"value": "val 3",
			"description": "desc 1",
			"accepted": true
		},
		{
			"item_type_id": "ac6e5fde-621e-4a4a-b44b-27ef33afd531",
			"value": "val 4",
			"description": "desc 2",
			"accepted": true
		}
	]
}
```
Response:
```json
{
    "code": 201,
    "message": "created",
    "data": []
}
```

#### Search for Items with Item value
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/search/items  
Body:
```json
{
	"channel_id": "401afb60-a2c0-4943-a470-5cce1ea9218b",
	"item_value": "val 3"
}
```
Response:
```json
{
    "code": 200,
    "data": [
        {
            "id": "92f2d3fc-a8b7-4a58-9850-41f2e1f629ad",
            "type": "email",
            "value": "val 3",
            "description": "desc 1",
            "accepted": true,
            "created_at": "2019-09-29",
            "updated_at": "2019-09-29"
        }
    ]
}
```

#### Delete one or more Items
Content-Type: application/json  
Method: DELETE  
Endpoint: http://{{base_url}}/api/items-delete  
Body:
```json
{
	"user_id": "464f9a71-88de-4424-805b-caf348bf9e23",
	"item_values": [
		"val 3"
	]
}
```
Response:
```json
{
    "code": 200,
    "message": "removed",
    "data": []
}
```

#### Updating an Item
Content-Type: application/json  
Method: PATCH  
Endpoint: http://{{base_url}}/api/items  
Body:
```json
{
	"channel_id": "401afb60-a2c0-4943-a470-5cce1ea9218b",
	"item_value": "Item value 2",
	"description": "desc 1 updated",
	"accepted": true
}
```
Response:
```json
{
    "code": 200,
    "message": "updated",
    "data": []
}
```

### ItemType management

#### Create an ItemType
Content-Type: application/json  
Method: POST  
Endpoint: http://{{base_url}}/api/item-types  
Body:
```json
{
	"project_id": "43501cf5-6ce8-464a-92e6-0ddde4757985",
	"item_types": [
		{
			"name": "Email"
		},
		{
			"name": "Phone"
		}
	]
}
```
Response:
```json
{
    "code": 201,
    "message": "created",
    "data": [
        {
            "created": [
                {
                    "id": "63092ff9-099d-49fb-a337-56b28805c2c2",
                    "name": "Email"
                },
                {
                    "id": "912e13ec-658d-4697-8d6e-3959c3cc3a8f",
                    "name": "Phone"
                }
            ],
            "already_exists": []
        }
    ]
}
```

#### List all ItemType by Project id
Content-Type: application/json  
Method: GET  
Endpoint: http://{{base_url}}/api/item-types/<project id>  
Response:
```json
{
    "code": 200,
    "data": [
        [
            {
                "id": "63092ff9-099d-49fb-a337-56b28805c2c2",
                "name": "email"
            },
            {
                "id": "648ffb9f-2bd8-4a6a-949d-fef8c44f439f",
                "name": "phone"
            }
        ]
    ]
}
```

