#!/bin/sh

set -e

cd "${0%/*}/.."

echo "##### Running PHPStan..."
docker exec -i php sh -c "vendor/phpstan/phpstan/bin/phpstan analyse src --level=4"