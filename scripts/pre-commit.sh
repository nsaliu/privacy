#!/bin/sh

echo "Running pre-commit hook"
./scripts/run-php_stan.sh
./scripts/run-cs_fixer.sh
#./scripts/run-tests.sh

if [ $? -ne 0 ]; then
 echo "########## There are some problems...fix before commit ;-)"
 exit 1
fi