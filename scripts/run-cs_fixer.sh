#!/bin/sh

set -e

cd "${0%/*}/.."

echo "##### Running CS-Fixer..."
docker exec -i php sh -c "vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix src"