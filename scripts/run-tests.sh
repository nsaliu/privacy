#!/bin/sh

set -e

cd "${0%/*}/.."

echo "##### Running tests..."
docker exec -i php sh -c "bin/phpunit"